<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class Rate extends Model
{
    protected $fillable = [
        'id','rater_id','owner_id','lodge_id','overall',
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    public function submitRate($input){
        $objLodge = new Lodge();
        $lodge = $objLodge->where('ref_code',$input['ref_code'])->first(); 
        $overall = 0 ;
        foreach ($input['rate'] as $index=>$obj){
            $obj1 = explode("{", $obj);
            $obj2 = explode("}", $obj1[1]);
            $obj3 = explode("=", $obj2[0]);
           $overall = $overall + $obj3[1];
        }
            $overall = $overall/3 ;
            $obj = new Rate();
            $objRateBadges = new RatesBadge();
            $res =  $obj->updateOrCreate(['rater_id'=> Auth::user()->id,'lodge_id'=>$lodge['id']],['owner_id'=>$input['user_id'],'overall'=>$overall]);
        foreach ($input['rate'] as $index=>$obj){
           $obj1 = explode("{", $obj);
           $obj2 = explode("}", $obj1[1]);
           $obj3 = explode("=", $obj2[0]);
           $res2 = $objRateBadges->updateOrCreate(['rate_id'=> $res['id'],'badge_id'=>$obj3[0]],['badge_id'=>$obj3[0],'value'=>$obj3[1]]);
       }
           return Status::printStatus(200);
    }
}
