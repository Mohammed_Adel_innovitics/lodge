<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class Packagetransaction extends Model
{
    protected $fillable = [
        'id', 'package_id','lodge_id','status_id','start','end','remaining_days','fort_id','notes'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    public function addTransaction($packageId,$lodgeId,$status_id,$start,$end,$days,$fort_id,$note){
        $this->package_id = $packageId ; 
        $this->lodge_id = $lodgeId ;
        $this->status_id = $status_id ; 
        $this->start = $start ;
        $this->end = $end ; 
        $this->remaining_days = $days ; 
        $this->fort_id = $fort_id ; 
        $this->note = $note;
        $this->save();
        return $this ; 
    }
    
    public function getPackageByLodge($lodgeId){
        $res = $this->where('lodge_id',$lodgeId)->orderBy('updated_at', 'desc')->first();
        return $res ; 
    }
    
    public function getValidLodges(){
        $arrLodgeIds = $this->where('status_id',2)->where('end','>=',Carbon::now()->toDateString())->pluck('lodge_id')->toArray();
        return $arrLodgeIds ;
    }
    
    public function getLastInReview($lodge_id){
        $res = $this->where('status_id',1)->where('lodge_id',$lodge_id)->orderBy('updated_at', 'desc')->first();
        return $res ; 
    }
    
    public function getLodgeDuration($lodge_id){
            $trans = $this->where('lodge_id',$lodge_id)->where('status_id',2)->get();
            $start = $trans->first()['start'];
            $end = $trans->last()['end'];
            if($start == null){
                
            }else{
            return 'from: '.Carbon::parse($start)->toFormattedDateString().' to: '.Carbon::parse($end)->toFormattedDateString();
            }
    }
}
