<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Intro extends Model
{
    use Translatable;
    
    use SoftDeletes;
    
    protected $fillable = [
        'id', 'text', 'img'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['text'];
    
    
}
