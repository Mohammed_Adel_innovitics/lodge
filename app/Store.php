<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Spatial;

class Store extends Model
{
    use Translatable,SoftDeletes,Spatial;
    protected $spatial = ['coordinates'];
    protected $fillable = [
        'id', 'name','tags', 'logo', 'featured_img', 'address', 'phone', 
        'website', 'facebook', 'youtube', 'instagram', 'desc', 'coordinates'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name', 'desc'];
    
    public function getStoresByCatId($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $objStore = new Store();
        $arrStoreCategories = $objStore->select('stores.id','stores.name','stores.logo','stores.tags')
        ->join('store_categories','stores.id','store_categories.store_id')
        ->where('cat_id', $request['cat_id'])->paginate(5);
        
        $arrStoreCategoriesTrans = $arrStoreCategories->translate($lang,'en');
        
        foreach ($arrStoreCategoriesTrans as $index=>$obj){
            $arrStoreCategoriesTrans[$index]['tags'] = json_decode($arrStoreCategoriesTrans[$index]['tags']);
            $arrStoreCategoriesTrans[$index]['logo'] = str_replace('\\', '/', MediaUrl::getUrl().$arrStoreCategoriesTrans[$index]['logo']);
        }
        
        $arrStore = translationHelper::translatedCollectionToArray($arrStoreCategoriesTrans);
        $arrStore = translationHelper::paginationForTranslator($arrStoreCategories, $arrStoreCategoriesTrans);
        
        return $arrStore;
    }
    
    public function getStoreById($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $store = $this->where('id', $request['store_id'])->get()->translate($lang,'en');
        
//         $store['logo'] = str_replace('\\', '/', MediaUrl::getUrl().$store['logo']);
        $store[0]['featured_img'] = str_replace('\\', '/', MediaUrl::getUrl().$store[0]['featured_img']);
        $store[0]['coordinates'] = $this->where('id', $request['store_id'])->get()[0]->getCoordinates();
        $store[0]['tags'] = json_decode($store[0]['tags']);
        return $store;
    }
    
    public function filterStores($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $arrStores = $this->select('id','name','logo','tags')->where('name', 'like', '%'.$request['name'].'%')->get()->translate($lang, 'en');
        
        foreach ($arrStores as $index=>$obj){
            $arrStores[$index]['logo'] = str_replace('\\', '/', MediaUrl::getUrl().$arrStores[$index]['logo']);
        }
        
        return $arrStores;
    }
}
