<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Validator;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','fb_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function getUser($intUserID){
        
        $user = $this->where('id',$intUserID)->first();
        
        if(str_contains($user->avatar, 'graph.facebook')){
            
            //             dd('graph');
            
        }
        
        else{
            
            if($user->avatar != null){
                
                $user->avatar = MediaUrl::geturl().$user->avatar;
                
            }
            
        }
        
        return $user;
        
    }
    
    public function editProfile($request){
        $arr = array();
        $user = Auth::user() ;
        if(isset($request['name']) && $request['name']!= null && $request['name']!=""){
//             $this->where('id',Auth::user()->id)->update(['name'=>$request['name']]);
            $user->name = $request['name'];
        }
        if(isset($request['phone']) && $request['phone']!= null && $request['phone']!=""){
                $phoneCheck = $this->where('id',Auth::user()->id)->where('phone',$request['phone'])->first();
                if($phoneCheck){
                    
                }else{
                    
                    $validator = Validator::make($request->all(), [
                        'phone'=>'unique:users',
                    ]);
                    if ($validator->fails()) {
                        
                        $error = $validator->errors();
                        if($error->first('phone')){
                            $arr = Status::mergeStatus($arr,4020,$request['lang_id']);
                            return $arr;
                        }
                    }
//                     $this->where('id',Auth::user()->id)->update(['phone'=>$request['phone']]);
                    $user->phone = $request['phone'];
                    
                }    
        }
        if(isset($request['password']) && $request['password']!= null && $request['password']!=""){
            $validator = Validator::make($request->all(), [
                'password'=> 'min:6|max:50',
            ]);
            if ($validator->fails()) {
                
                $error = $validator->errors();
                if($error->first('password')){
                    $arr = Status::mergeStatus($arr,4019,$request['lang_id']);
                    return $arr;
                }
            }
//             $this->where('id',Auth::user()->id)->update(['password'=>bcrypt($request['password'])]);
            $user->password = bcrypt($request['password']);
            
        }
        $user->save();
        $success['user'] = $user;
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $arr['results'] = $success;
        return Status::mergeStatus($arr,200);
    }
}
