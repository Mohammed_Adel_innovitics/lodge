<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Package extends Model
{
    use SoftDeletes;
    use Translatable;
    protected $fillable = [
        'id', 'days', 'price','title','desc','is_featured','price_visible'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['title','desc'];
}
