<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tip;
use App\Status;
use App\Lodge;

class TipController extends Controller
{   
    public function GetConfig(Request $request){
        $lang = $request['lang_id'];
        $objTip = new Tip();
        $arr = array();
        $last = array();
        $result['Rent']['price']=  $objTip->getPrice('R');
        $result['Sale']['price'] = $objTip->getPrice('S');
        $result['Rent']['bedrooms'] = $objTip->getBedrooms('R');
        $result['Sale']['bedrooms'] = $objTip->getBedrooms('S');
        $result['Rent']['bathrooms'] = $objTip->getBathrooms('R');
        $result['Sale']['bathrooms'] = $objTip->getBathrooms('S');
        $result['Rent']['size'] = $objTip->getSize('R');
        $result['Sale']['size'] = $objTip->getSize('S');
        
        $last = array_merge($last,$result['Rent']['price']);
        $last = array_merge($last,$result['Sale']['price']);
        $last = array_merge($last,$result['Rent']['bedrooms']);
        $last = array_merge($last,$result['Sale']['bedrooms']);
        $last = array_merge($last,$result['Rent']['bathrooms']);
        $last = array_merge($last,$result['Sale']['bathrooms']);
        $last = array_merge($last,$result['Rent']['size']);
        $last = array_merge($last,$result['Sale']['size']);
        
        $last['tips'] = $objTip->OrderBy('id','ASC')->get(['id','sentence'])->translate($lang,'en')->toArray();
        $arr['result'] = $last;
        return Status::mergeStatus($arr,200);
    }
}
