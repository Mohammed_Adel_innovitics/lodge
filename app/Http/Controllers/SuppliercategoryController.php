<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suppliercategory;
use App\Status;

class SuppliercategoryController extends Controller
{
    public function GetSupplierCategories(Request $request){
        $arr = array();
        $objSupplierCat = new Suppliercategory();
        $arrSupplierCategories = $objSupplierCat->getSupplierCategories($request);
        $arr['result'] = $arrSupplierCategories ;
        
        return Status::mergeStatus($arr,200);
    }
}
