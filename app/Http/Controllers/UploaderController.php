<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Status;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use App\Lodge;

class UploaderController extends Controller
{
    //
    public function DeleteLodgeImage(Request $request){
        $arr = array();
        $user_id = Auth::user()->id ;
        $file = new Filesystem();
        $path = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/tmp';
        $file->cleanDirectory($path);
        return Status::mergeStatus($arr,200);
    }

    
    public function UploadLodgeImage(Request $request){
        $arr = array();
        $user_id = Auth::user()->id ;
        if($request->type == 'F'){
            $path = $user_id.'/'.$request['ref_code'].'/tmp'.'/floor/';
        }else{
            $path = $user_id.'/'.$request['ref_code'].'/tmp'.'/gallery/';
        }
        $fileName = pathinfo($request->file('image')->getClientOriginalName(), PATHINFO_FILENAME).time().'.'.$request->file('image')->getClientOriginalExtension();
        $destinationPath_original = storage_path('app/private/images/').$path;
        $photoSave = $request->file('image')->move($destinationPath_original, $fileName);
        if($photoSave){
            $arr = Status::mergeStatus($arr,200);
        }else{
            $arr = Status::mergeStatus($arr,4012);
        }
        return $arr ;
    }
    public function moveFromTmpToMain($refCode,$user_id){
        $file = new Filesystem();
        $path = storage_path('app/private/images/').$user_id.'/'.$refCode.'/gallery';
        $path2 = storage_path('app/private/images/').$user_id.'/'.$refCode.'/floor';
        $file->cleanDirectory($path);
        $file->cleanDirectory($path2);
        $fromPath = storage_path('app/private/images/').$user_id.'/'.$refCode.'/tmp';
        $ToPath = storage_path('app/private/images/').$user_id.'/'.$refCode;
        $copied = \File::copyDirectory($fromPath,$ToPath);
        $galleryPath = storage_path('app/private/images/').$user_id.'/'.$refCode.'/gallery/';
        $floorPath = storage_path('app/private/images/').$user_id.'/'.$refCode.'/floor/';
        $floorImage = glob($floorPath. "*");
        $galleryImages = glob($galleryPath."*");
        if(count($floorImage) > 0){
            foreach ($floorImage as $index=>$image){
                $floorImage[$index] = explode('images/',$floorImage[$index]);
                $floorImage[$index]= $floorImage[$index][1];
                $img [] = $floorImage[$index] ;
                
            }
            $floor = json_encode($img);
            $objLodge = new Lodge();
            $update = $objLodge->where('ref_code',$refCode)->update(['floor_plan'=>$floor]);
        }
        if(count($galleryImages) > 0){
            foreach ($galleryImages as $index=>$image){
                $galleryImages[$index] = explode('images/',$galleryImages[$index]);
                $galleryImages[$index]= $galleryImages[$index][1];
                $img2 [] = $galleryImages[$index] ;
            }
            $gallery = json_encode($img2);
            $objLodge = new Lodge();
            $update = $objLodge->where('ref_code',$refCode)->update(['img'=>$gallery]);
        }
    }
}
