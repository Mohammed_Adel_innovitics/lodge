<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;
use Illuminate\Support\Facades\Auth;
use App\Status;
use Illuminate\Support\Facades\DB;
use App\Lodge;

class LikeController extends Controller
{
    //
    public function SubmitLike(Request $request){
        $objlike = new Like();
        $userId  = Auth::user()->id;
        $res = DB::table('likes')->updateOrInsert(['lodge_id'=>$request['lodge_id'],'user_id'=>$userId],['value'=>$request['value']]);
        $nooflikes = $objlike->where('lodge_id',$request['lodge_id'])->where('value','1')->count();
        $objLodge = new Lodge();
        $update = $objLodge->where('id',$request['lodge_id'])->update(['count_like'=>$nooflikes]);
        return Status::printStatus(200);
    }
}
