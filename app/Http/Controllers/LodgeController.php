<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Status;
use App\Lodge;
use Illuminate\Support\Facades\Auth;
use App\Lodgeoption;
use Illuminate\Support\Facades\DB;
use App\Packagetransaction;
use App\MediaUrl;
use TCG\Voyager\Traits\Spatial;
use App\Like;
use App\UserSearch;
use Illuminate\Support\Facades\Log;
use App\Reason;
use App\Apntoken;
use App\Badge;
use Carbon\Carbon;
use App\Compound;
use App\Area;
use App\Governrate;
use App\Period;
use App\Currency;
use App\Unittype;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Filesystem\Filesystem;
use App\Feature;
class LodgeController extends VoyagerBaseController
{
    //
    use Spatial;
    use BreadRelationshipParser;
    public function AddLodge(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'process'=>'required',
            'unit_type_id'=> 'required|exists:unittypes,id',
            'bedrooms'=> 'required_unless:unit_type_id,8|max:2',
            'bathrooms'=> 'required_unless:unit_type_id,8|max:2',
            'size'=> 'required_unless:unit_type_id,8|max:6',
            'price'=> 'required|max:8',
            'currency_id'=> 'required|exists:currencies,id',
            'governrate_id'=> 'required|exists:governrates,id',
            'area_id'=> 'required|exists:areas,id',
            'title'=> 'required',
            'package_id'=> 'required|exists:packages,id',
            'ref_code'=>'required',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('process')){
                $arr = Status::mergeStatus($arr,100,$request['lang_id']);
            }if($error->first('unit_type_id')){
                $arr = Status::mergeStatus($arr,101,$request['lang_id']);
            }if($error->first('bedrooms')){
                $arr = Status::mergeStatus($arr,99,$request['lang_id']);
            }if($error->first('bathrooms')){
                $arr = Status::mergeStatus($arr,201,$request['lang_id']);
            }if($error->first('size')){
                $arr = Status::mergeStatus($arr,202,$request['lang_id']);
            }if($error->first('price')){
                $arr = Status::mergeStatus($arr,203,$request['lang_id']);
            }if($error->first('currency_id')){
                $arr = Status::mergeStatus($arr,204,$request['lang_id']);
            }if($error->first('governrate_id')){
                $arr = Status::mergeStatus($arr,206,$request['lang_id']);
            }if($error->first('area_id')){
                $arr = Status::mergeStatus($arr,300,$request['lang_id']);
            }if($error->first('title')){
                $arr = Status::mergeStatus($arr,301,$request['lang_id']);
            }if($error->first('package_id')){
                $arr = Status::mergeStatus($arr,303,$request['lang_id']);
            }if($error->first('ref_code')){
                $arr = Status::mergeStatus($arr,5007,$request['lang_id']);
            }
            return $arr;
            
        }
        $user_id =  Auth::user()->id ;
        $galleryPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/tmp'.'/gallery/';
        $floorPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/tmp'.'/floor/';
        $floorImage = glob($floorPath. "*");
        $galleryImages = glob($galleryPath."*");
        if(count($floorImage) > 0){
            foreach ($floorImage as $index=>$image){
                $floorImage[$index] = explode('images/',$floorImage[$index]);
                $floorImage[$index]= $floorImage[$index][1];
                $img [] = $floorImage[$index] ; 
                
            }
            $floor = json_encode($img);
//             $request['floor'] = $floor ; 
        }
        
        if(count($galleryImages) > 0){
            foreach ($galleryImages as $index=>$image){
                $galleryImages[$index] = explode('images/',$galleryImages[$index]);
                $galleryImages[$index]= $galleryImages[$index][1];
                $img2 [] = $galleryImages[$index] ;
            }
            $gallery = json_encode($img2);
//             $request['gallery'] = $gallery ; 
        }else{
            $arr = Status::mergeStatus($arr,5000,$request['lang_id']);
            return $arr ;
        }

        $objLodge = new Lodge();
        $res = $objLodge->addLodge($request);
        if(isset($request['arrOptions']) && count($request['arrOptions']) > 0){
            $objLodgeOption = new Lodgeoption();
            foreach ($request['arrOptions'] as $option){
            $saveOptions = $objLodgeOption->insert(['option_id'=>$option,'lodge_id'=>$res['id']]);
            }
        }
           if($request['package_id'] == '3'){
           $objTrans = new Packagetransaction();
           $result = $objTrans->addTransaction($request['package_id'],$res['id'],'1',null,null,null,null,null);
           }
           $objUploader = new UploaderController();
           $saveImage = $objUploader->moveFromTmpToMain($request['ref_code'], $user_id);
           $arr['result'] = $res ; 
           $arr = Status::mergeStatus($arr,200);
            return $arr ; 
    }
    
    public function ListLatest(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        if($request['type'] == 'R' || $request['type'] == 'S'){
            
            $objLodge = new Lodge();
            $input = $request->all();
            $arrLodges = $objLodge->listLatest($input);
            $arr['result'] = $arrLodges;
            return Status::mergeStatus($arr,200);
        }else{
            return Status::mergeStatus($arr,100,$request['lang_id']);
            
        }
    }
    
    public function ListLodgesByUser(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $objLodge = new Lodge();
        $arrLodges = $objLodge->listLodgesByUser($request['user_id'],$request['limit'],$request['lang_id']);
        $arr['result'] = $arrLodges;
        return Status::mergeStatus($arr,200);
    }
    
    public function AddView(Request $request){
        $objLodge = new Lodge();
        $update = DB::table('lodges')->where('id',$request['lodge_id'])->increment('count_views',1);
        return Status::printStatus(200);
    }
    
    public function SearchLodge(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        Log::debug($request);
        $arr = array();
        if($request['process'] == 'R' || $request['process'] == 'S'){
        $input = $request->all();
        $objLodge = new Lodge();
        if(Auth::user() == null){
        }else{
            $objUserSearch = new UserSearch();
            $save = $objUserSearch->saveSearch($input);
        }
        $arrLodges = $objLodge->searchLodge($input);
        $arr['result'] = $arrLodges;
        return Status::mergeStatus($arr,200);
        }else{
            return Status::mergeStatus($arr,100,$request['lang_id']);
        }
    }
    
    public function ListFavLodges(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $objLike = new Like();
        $arrLodgeIds = $objLike->getFavLodges(Auth::user()->id);
        $objLodge = new Lodge();
        $arrLodges = $objLodge->whereIn('id',$arrLodgeIds)->whereIn('status_id',['2','5','7'])->paginate(5);
        $lodges = $objLodge->getLodgeDetails($arrLodges,$request['lang_id']);
        $arr['result'] = $lodges;
        $arr = Status::mergeStatus($arr,200);
        return $arr ;
    }
    
    public function ListMyLodges(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $objLodge = new Lodge();
        $arrLodges = $objLodge->listMyLodges($request['lang_id']);
        $arr['result'] = $arrLodges ;
        $arr = Status::mergeStatus($arr,200);
        return $arr ;
    }
    
    public function DeactivateLodge(Request $request){
        $arr = array();
        $objReason = new Reason();
        $note = $objReason->where('id',$request['reason_id'])->get(['name'])->toArray()[0]['name'];
        $objLodge = new Lodge();
        $objTrans = new Packagetransaction();
        $lodgePackage = $objTrans->getPackageByLodge($request['lodge_id']);
        if($request['reason_id'] == 1){
        $status = DB::table('statuses')->where('id','5')->first();
        $result = $objTrans->addTransaction($lodgePackage['package_id'],$lodgePackage['lodge_id'],'5',null,null,null,null,$note);
        $res = $objLodge->where('id',$request['lodge_id'])->update(['status_id'=>'5']);
        }else{
            $status = DB::table('statuses')->where('id','7')->first();
            $result = $objTrans->addTransaction($lodgePackage['package_id'],$lodgePackage['lodge_id'],'7',null,null,null,null,$note);
            $res = $objLodge->where('id',$request['lodge_id'])->update(['status_id'=>'7']);
        }
        $refCode = DB::table('lodges')->where('id',$request['lodge_id'])->first();
        $objApn = new Apntoken();
        $notifi = $objApn->sendNotifi(Auth::user()->id,'Lodge','Your Lodge #'.$refCode->ref_code.' is '.$status->name);
        return Status::printStatus(200);
    }
    
    public function EditLodge(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'process'=>'required',
            'unit_type_id'=> 'required|exists:unittypes,id',
            'bedrooms'=> 'required_unless:unit_type_id,8|max:2',
            'bathrooms'=> 'required_unless:unit_type_id,8|max:2',
            'size'=> 'required_unless:unit_type_id,8|max:6',
            'price'=> 'required|max:8',
            'currency_id'=> 'required|exists:currencies,id',
            'governrate_id'=> 'required|exists:governrates,id',
            'area_id'=> 'required|exists:areas,id',
            'title'=> 'required',
            'ref_code'=>'required',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('process')){
                $arr = Status::mergeStatus($arr,100,$request['lang_id']);
            }if($error->first('unit_type_id')){
                $arr = Status::mergeStatus($arr,101,$request['lang_id']);
            }if($error->first('bedrooms')){
                $arr = Status::mergeStatus($arr,99,$request['lang_id']);
            }if($error->first('bathrooms')){
                $arr = Status::mergeStatus($arr,201,$request['lang_id']);
            }if($error->first('size')){
                $arr = Status::mergeStatus($arr,202,$request['lang_id']);
            }if($error->first('price')){
                $arr = Status::mergeStatus($arr,203,$request['lang_id']);
            }if($error->first('currency_id')){
                $arr = Status::mergeStatus($arr,204,$request['lang_id']);
            }if($error->first('governrate_id')){
                $arr = Status::mergeStatus($arr,206,$request['lang_id']);
            }if($error->first('area_id')){
                $arr = Status::mergeStatus($arr,300,$request['lang_id']);
            }if($error->first('title')){
                $arr = Status::mergeStatus($arr,301,$request['lang_id']);
            }if($error->first('ref_code')){
                $arr = Status::mergeStatus($arr,5007,$request['lang_id']);
            }
            return $arr;
            
        }
        $user_id =  Auth::user()->id ;
        $galleryPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/tmp'.'/gallery/';
        $floorPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/tmp'.'/floor/';
        $floorImage = glob($floorPath. "*");
        $galleryImages = glob($galleryPath."*");
        if(count($floorImage) > 0){
            foreach ($floorImage as $index=>$image){
                $floorImage[$index] = explode('images/',$floorImage[$index]);
                $floorImage[$index]= $floorImage[$index][1];
                $img [] = $floorImage[$index] ;
                
            }
            $floor = json_encode($img);
//             $request['floor'] = $floor ;
        }
        
        if(count($galleryImages) > 0){
            foreach ($galleryImages as $index=>$image){
                $galleryImages[$index] = explode('images/',$galleryImages[$index]);
                $galleryImages[$index]= $galleryImages[$index][1];
                $img2 [] = $galleryImages[$index] ;
            }
            $gallery = json_encode($img2);
//             $request['gallery'] = $gallery ;
        }else{
            $arr = Status::mergeStatus($arr,5000,$request['lang_id']);
            return $arr ;
        }
        
        $objLodge = new Lodge();
        $res = $objLodge->editLodge($request);
        $objTrans = new Packagetransaction();
        $lodgePackage = $objTrans->getPackageByLodge($request['lodge_id']);
        $result = $objTrans->addTransaction($lodgePackage['package_id'],$lodgePackage['lodge_id'],'1',null,null,null,null,null);
        $res2 = $objLodge->where('id',$request['lodge_id'])->update(['status_id'=>'1']);
        if(isset($request['arrOptions']) && count($request['arrOptions']) > 0){
            $objLodgeOption = new Lodgeoption();
            $deleteOptions = $objLodgeOption->where('lodge_id',$request['lodge_id'])->delete();
            foreach ($request['arrOptions'] as $option){
                $saveOptions = $objLodgeOption->insert(['option_id'=>$option,'lodge_id'=>$request['lodge_id']]);
            }
        }
        $objUploader = new UploaderController();
        $saveImage = $objUploader->moveFromTmpToMain($request['ref_code'], $user_id);
        $arr = Status::printStatus(200);
        return $arr ;
    }
    
    public function GetLodge(Request $request){
        $objLodge = new Lodge();
        (isset($request['ref_code'])) ? $request['lodge_id'] = $objLodge->where('ref_code',$request['ref_code'])->first(['id'])['id']
        : $request['lodge_id'] ;
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $objLodgeOption = new Lodgeoption();
        if(Auth::user() != null){
            //check if user is the owner
//             dd($objLodge->where('id',$request['lodge_id'])->first()['user_id']);
            if(Auth::user()->id == $objLodge->where('id',$request['lodge_id'])->first()['user_id']){
                $query = $objLodge->where('lodges.id',$request['lodge_id']);
            }else{
                $query = $objLodge->where('lodges.id',$request['lodge_id'])->where('status_id',2);
            }
            //check if user is the owner
            
            $lodge = $query->leftjoin('rates',function ($join){
                $join->on('rates.lodge_id', '=', 'lodges.id')
                ->where('rates.rater_id',Auth::user()->id);
            })->select("lodges.*",
                DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = '.Auth::user()->id.' THEN "1"
                        ELSE "0"
                        END) AS is_rated '))->first();;
        }else{
            $lodge =$objLodge->where('lodges.id',$request['lodge_id'])->where('status_id',2)->leftjoin('rates',function ($join){
                $join->on('rates.lodge_id', '=', 'lodges.id')
                ->where('rates.rater_id','0');
            })->select("lodges.*",
                DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = 0 THEN "1"
                        ELSE "0"
                        END) AS is_rated '))->first();;
        }
        if($lodge == null){
            $arr = array();
            return Status::mergeStatus($arr,5018,$request['lang_id']);
        }
//         $lodge = $objLodge->where('id',$request['lodge_id'])->first();
        $lodge['total_rate'] = floatval(number_format(DB::table('rates')->where('owner_id',$lodge['user_id'])->avg('overall'),1));
        if($lodge['total_rate'] == null){
            $lodge['total_rate'] = 0 ;
        }
        if($lodge['img'] != null){
            $lodge['img'] = json_decode($lodge['img']);
            $arrImg = array();
            $arrPlan = array();
            foreach ($lodge['img'] as $index2=>$img){
                $arrImg [] = str_replace('\\', '/', MediaUrl::getImage().$lodge['img'][$index2]);
            }
            $lodge['img'] = $arrImg;
        }
        if($lodge['floor_plan']!= null){
            $lodge['floor_plan'] = json_decode($lodge['floor_plan']);
            foreach ($lodge['floor_plan'] as $index2=>$img){
                $arrPlan [] = str_replace('\\', '/', MediaUrl::getImage().$lodge['floor_plan'][$index2]);
            }
            $lodge['floor_plan'] = $arrPlan;
        }
        $lodge['coordinates'] = $lodge->getCoordinates();
        $objUnitType = new Unittype();
        $objCurr = new Currency();
        $objPeriod = new Period();
        $objGov = new Governrate();
        $objArea = new Area();
        $objComp = new Compound();
        $objStatus = new Status();
        $objBadge = new Badge();
        $objLike = new Like();
        $objTrans = new Packagetransaction();
        $lodge['unit_type_id'] = $objUnitType->where('id',$lodge['unit_type_id'])->first(['id','name'])->translate($request['lang_id'],'en');
        $lodge['currency_id'] = $objCurr->where('id',$lodge['currency_id'])->first(['id','name'])->translate($request['lang_id'],'en');
        if($lodge['period_id']!= null){
            $lodge['period_id'] = $objPeriod->where('id',$lodge['period_id'])->first(['id','name'])->translate($request['lang_id'],'en');
        }
        $lodge['governrate_id'] = $objGov->where('id',$lodge['governrate_id'])->first(['id','name'])->translate($request['lang_id'],'en');
        $lodge['area_id'] = $objArea->where('id',$lodge['area_id'])->first(['id','name','governrate_id'])->translate($request['lang_id'],'en');
        ($lodge['compound_id'] != null) ? $lodge['compound_id'] = $objComp->where('id',$lodge['compound_id'])->first(['id','name','area_id'])->translate($request['lang_id'],'en') : null ;
        
        $deactivate_flag = ($lodge['status_id'] == 2) ? 1 : 0 ;
        $lodge['deactivate_flag'] = $deactivate_flag ;
        
        $repost_flag = ($lodge['status_id'] == 3 || $lodge['status_id'] == 5 || $lodge['status_id'] == 7) ? 1 : 0 ;
        $lodge['repost_flag'] = $repost_flag ;
        
        
        $lodge['status_id'] = $objStatus->where('id',$lodge['status_id'])->first(['id','name','color'])->translate($request['lang_id'],'en');
        $lodge['badges'] = $objBadge->getBadgeByUser($lodge['user_id'],$request['lang_id']);
        if(Auth::user() == null){
            $lodge['channel_name'] = null;
        }else{
            $lodge['channel_name'] = Auth::user()->id.'-'. $lodge['user_id'].'-'.$lodge['ref_code'];
        }
        $lodge['user_id'] = DB::table('users')->where('id',$lodge['user_id'])->first(['id','email','phone','name']);
        if(Auth::user() == null){
        }else{
        $isLike = $objLike->isLike($lodge['id'],Auth::user()->id);
        $lodge['isLike'] = $isLike ;
        }
        Carbon::setLocale($request['lang_id']);
        $lodge['created'] = Carbon::now()->subHour(Carbon::now()->diffInHours($lodge['created_at']))->diffForHumans();
        $lodge['duration'] = $objTrans->getLodgeDuration($lodge['id']);
        $lodge['Features'] = $objLodgeOption->getChosenOptions($request['lodge_id'],$request['lang_id']);
        
        $arr['result'] = $lodge;
        return Status::mergeStatus($arr,200);
    }
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        //         $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        if($request->hasfile('img') && count($request->file('img')) >0){
            $path = $request['user_id'].'/'.$request['ref_code'].'/gallery/';
            foreach ($request->file('img') as $objImage){
                $fileName = pathinfo($objImage->getClientOriginalName(), PATHINFO_FILENAME).'-'.time().'.'.$objImage->getClientOriginalExtension();
                $destinationPath_original = storage_path('app/private/images/').$path;
                $photoSave = $objImage->move($destinationPath_original, $fileName);
            }
        }
        if($request->hasfile('floor_plan') && $request->file('floor_plan') !=null){
            $path = $request['user_id'].'/'.$request['ref_code'].'/floor/';
            $fileName = pathinfo($request->file('floor_plan')->getClientOriginalName(), PATHINFO_FILENAME).'-'.time().'.'.$request->file('floor_plan')->getClientOriginalExtension();
            $destinationPath_original = storage_path('app/private/images/').$path;
            $photoSave = $request->file('floor_plan')->move($destinationPath_original, $fileName);
        }
        $save = $this->AddLodgeAp($request);
        //         event(new BreadDataAdded($dataType, $data));
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
            ]);
    }
    public function AddLodgeAp(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $user_id =  $request['user_id'] ;
        $galleryPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/gallery/';
        $floorPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/floor/';
        $floorImage = glob($floorPath. "*");
        $galleryImages = glob($galleryPath."*");
        if(count($floorImage) > 0){
            foreach ($floorImage as $index=>$image){
                $floorImage[$index] = explode('images/',$floorImage[$index]);
                $floorImage[$index]= $floorImage[$index][1];
                $img [] = $floorImage[$index] ;
                
            }
            $floor = json_encode($img);
            $request['floor'] = $floor ;
        }
        
        if(count($galleryImages) > 0){
            foreach ($galleryImages as $index=>$image){
                $galleryImages[$index] = explode('images/',$galleryImages[$index]);
                $galleryImages[$index]= $galleryImages[$index][1];
                $img2 [] = $galleryImages[$index] ;
            }
            $gallery = json_encode($img2);
            $request['gallery'] = $gallery ;
        }
        $objLodge = new Lodge();
        $res = $objLodge->addLodgeAp($request);
        if(isset($request['feature']) && count($request['feature']) > 0){
            $objLodgeOption = new Lodgeoption();
            foreach ($request['feature'] as $option){
                $saveOptions = $objLodgeOption->insert(['option_id'=>$option,'lodge_id'=>$res['id']]);
            }
        }
        $objTrans = new Packagetransaction();
        $result = $objTrans->addTransaction('3',$res['id'],'1',null,null,null,null,null);
        $arr['result'] = $res ;
        $arr = Status::mergeStatus($arr,200);
        return $arr ;
    }
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        
        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }
        
        // Check permission
        $this->authorize('edit', $data);
        
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
//         $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        $ref = DB::table('lodges')->where('id',$id)->first();
        $user_id = $request['user_id'] ;
        $request['ref_code'] = $ref->ref_code;
        $request['lodge_id'] = $id;
        $file = new Filesystem();
        $path = storage_path('app\/private\/images\/').$user_id.'\/'.$ref->ref_code;
        $file->cleanDirectory($path);
        if($request->hasfile('img') && count($request->file('img')) >0){
            $path = $request['user_id'].'/'.$ref->ref_code.'/gallery/';
            foreach ($request->file('img') as $objImage){
                $fileName = pathinfo($objImage->getClientOriginalName(), PATHINFO_FILENAME).'-'.time().'.'.$objImage->getClientOriginalExtension();
                $destinationPath_original = storage_path('app/private/images/').$path;
                $photoSave = $objImage->move($destinationPath_original, $fileName);
            }
        }
        if($request->hasfile('floor_plan') && $request->file('floor_plan') !=null){
            $path = $request['user_id'].'/'.$ref->ref_code.'/floor/';
            $fileName = pathinfo($request->file('floor_plan')->getClientOriginalName(), PATHINFO_FILENAME).'-'.time().'.'.$request->file('floor_plan')->getClientOriginalExtension();
            $destinationPath_original = storage_path('app/private/images/').$path;
            $photoSave = $request->file('floor_plan')->move($destinationPath_original, $fileName);
        }
        $update = $this->EditLodgeAp($request);
        if(isset($request['feature']) && count($request['feature']) > 0){
            $objLodgeOption = new Lodgeoption();
            $deleteOptions = $objLodgeOption->where('lodge_id',$id)->delete();
            foreach ($request['feature'] as $option){
                $saveOptions = $objLodgeOption->insert(['option_id'=>$option,'lodge_id'=>$id]);
            }
        }
        event(new BreadDataUpdated($dataType, $data));
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
            ]);
    }
    public function EditLodgeAp(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $user_id =  $request['user_id'] ;
        $galleryPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/gallery/';
        $floorPath = storage_path('app/private/images/').$user_id.'/'.$request['ref_code'].'/floor/';
        $floorImage = glob($floorPath. "*");
        $galleryImages = glob($galleryPath."*");
        if(count($floorImage) > 0){
            foreach ($floorImage as $index=>$image){
                $floorImage[$index] = explode('images/',$floorImage[$index]);
                $floorImage[$index]= $floorImage[$index][1];
                $img [] = $floorImage[$index] ;
                
            }
            $floor = json_encode($img);
            $request['floor'] = $floor ;
        }
        
        if(count($galleryImages) > 0){
            foreach ($galleryImages as $index=>$image){
                $galleryImages[$index] = explode('images/',$galleryImages[$index]);
                $galleryImages[$index]= $galleryImages[$index][1];
                $img2 [] = $galleryImages[$index] ;
            }
            $gallery = json_encode($img2);
            $request['gallery'] = $gallery ;
        }else{
            $arr = Status::mergeStatus($arr,5000,$request['lang_id']);
            return $arr ;
        }
        
        $objLodge = new Lodge();
        $currentLodge = DB::table('lodges')->where('id',$request['lodge_id'])->first();
        if($currentLodge != null){
            if($request['status_id'] != $currentLodge->status_id && $request['status_id'] == 2){
                $objpackageTrans = new Packagetransaction();
                $transInfo = $objpackageTrans->getPackageByLodge($request['lodge_id']);
                if($transInfo['status_id'] == 2){
                    $lastInReview = $objpackageTrans->getLastInReview($request['lodge_id']);
                    $remDays = Carbon::parse($transInfo['end'])->diffInDays($lastInReview['created_at']);
                    $add = $objpackageTrans->addTransaction($transInfo->package_id,$request['lodge_id'],2,Carbon::now()->toDateString(),Carbon::now()->addDays($remDays)->toDateString(),0,null,null);
                }else{
                    $add = $objpackageTrans->addTransaction($transInfo->package_id,$request['lodge_id'],2,Carbon::now()->toDateString(),Carbon::now()->addDays(60)->toDateString(),0,null,null);
                }
            }
        }
        $status = DB::table('statuses')->where('id',$request['status_id'])->first();
        $objApn = new Apntoken();
        $notifi = $objApn->sendNotifi($request['user_id'],'Lodge','Your Lodge #'.$request['ref_code'].' is '.$status->name);
        $res = $objLodge->editLodgeAp($request);
        $arr = Status::printStatus(200);
        return $arr ;
    }
    
    public function ReactivateLodge(Request $request){
        $arr = array();
        $objLodge = new Lodge();
        $objpackageTrans = new Packagetransaction();
        $currentLodge = DB::table('lodges')->where('id',$request['lodge_id'])->first();
        if($currentLodge != null && ($currentLodge->status_id == '3' || $currentLodge->status_id == '5' || $currentLodge->status_id == '7')){
            $repost = $objLodge->where('id',$currentLodge->id)->update(['status_id'=>'1']);
            $result = $objpackageTrans->addTransaction('3',$currentLodge->id,'1',null,null,null,null,null);
            $status = DB::table('statuses')->where('id','1')->first();
            $objApn = new Apntoken();
            $notifi = $objApn->sendNotifi(Auth::user()->id,'Lodge','Your Lodge #'.$currentLodge->ref_code.' is '.$status->name);
        }
        $arr = Status::printStatus(200);
        return $arr ;
    }
    public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;
        
        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        //get features
        $objFeat = new FeatureController();
        $arrFeatures = $objFeat->ListFeatures($request);
        $arrFeatures = $arrFeatures['result'];
        //get features
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','arrFeatures'));
    }
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }
        
        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');
        
        // Check permission
        $this->authorize('edit', $dataTypeContent);
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        //get features
        $objLodgeOp = new Lodgeoption();
        $arrFeatures = $objLodgeOp->getChosenOptions($id,'en');
//         dd($arrFeatures);
        //get features
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','arrFeatures'));
    }
}
