<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\Status;

class OfferController extends Controller
{
    //
    public function GetOffersByCatId(Request $request){
        $arr = Array();
        $objOffer = new Offer();
        $arrStoreCategories = $objOffer->getOffersByCatId($request);
        $arr['result'] = $arrStoreCategories;
        return Status::mergeStatus($arr,200);
    }
    
    public function GetOffersByStoreId(Request $request){
        $arr = Array();
        $objOffer = new Offer();
        $arrStoreCategories = $objOffer->getOffersByStoreId($request);
        $arr['result'] = $arrStoreCategories;
        return Status::mergeStatus($arr,200);
    }
    
    public function GetOfferById(Request $request){
        $arr = Array();
        $objOffer = new Offer();
        $offer = $objOffer->getOfferById($request);
        $arr['result'] = $offer;
        return Status::mergeStatus($arr, 200);
    }
}
