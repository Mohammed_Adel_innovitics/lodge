<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Developer;
use App\Status;

class DeveloperController extends Controller
{
    //
    public function GetDevelopers(Request $request){
        $arr = Array();
        $objDeveloper = new Developer();
        $arrDevelopers = $objDeveloper->getDevelopers($request);
        $arr['result'] = $arrDevelopers;
        
        return Status::mergeStatus($arr,200);
    }
    
    public function GetDeveloperById(Request $request){
        $arr = Array();
        $objDeveloper = new Developer();
        $developer = $objDeveloper->getDeveloperById($request);
        $arr['result'] = $developer;
        
        return Status::mergeStatus($arr, 200);
    }
}
