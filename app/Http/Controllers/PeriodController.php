<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;
use App\Status;

class PeriodController extends Controller
{
    //
    public function ListPeriods(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objPeriods = new Period();
        $arrPeriods = $objPeriods->get(['id','name','eq_days'])->translate($lang,'en')->toArray();
        foreach ($arrPeriods as $obj){
            $obj['id'] = intval($obj['id']);
        }
        $arr['result'] = $arrPeriods;
        $arr = Status::mergeStatus($arr,200);
        return $arr ;
    }
}
