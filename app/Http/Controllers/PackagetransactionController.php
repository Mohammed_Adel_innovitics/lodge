<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packagetransaction;

class PackagetransactionController extends Controller
{
    //
    public function Chrone(Request $request){
        $arr = array();
        $objTrans = new Packagetransaction();
        $arrrExpLodges  = $objTrans->get()->groupBy('lodge_id');
        foreach($arrrExpLodges as $index=>$trans){
            if($trans->last()->status_id == 2){
                $arr[] = $trans->last();
            }
        }
        return $arr;
    }
}
