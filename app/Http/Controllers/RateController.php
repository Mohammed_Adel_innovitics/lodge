<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;
use App\Status;
use Illuminate\Support\Facades\Log;

class RateController extends Controller
{
    //
    public function SubmitRate(Request $request){
        Log::debug($request);
        $arr = array();
        $input = $request->all();
        $objRate = new Rate();
        $res = $objRate->submitRate($input);
        return Status::mergeStatus($arr,200);
    }
}
