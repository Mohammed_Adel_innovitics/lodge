<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Status;
use Illuminate\Support\Facades\Log;
class StoreController extends Controller
{
    public function GetStoresByCatId(Request $request){
        Log::debug($request);
        $arr = Array();
        $objStore = new Store();
        $arrStoreCategories = $objStore->getStoresByCatId($request);
        $arr['result'] = $arrStoreCategories;
        
        return Status::mergeStatus($arr,200);
    }
    
    public function GetStoreById(Request $request){
        $arr = Array();
        $objStore = new Store();
        $store = $objStore->getStoreById($request);
        $arr['result'] = $store;
        
        return Status::mergeStatus($arr, 200);
    }
    
    public function FilterStores(Request $request){
        $arr = Array();
        $objStore = new Store();
        $arrStores = $objStore->filterStores($request);
        $arr['result'] = $arrStores;
        
        return Status::mergeStatus($arr, 200);
    }
}
