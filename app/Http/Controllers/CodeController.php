<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Code;
use App\Status;
use Carbon\Carbon;
class CodeController extends Controller
{
    //
    public function GenerateCode(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $objCode = new Code();
        $curr = $objCode->checkCurrentCode($request['phone']);
        if( $curr== 'true'){
        $checkLogs = $objCode->checkLogs($request['phone']);
        if($checkLogs == false){
            return Status::mergeStatus($arr,5017,$request['lang_id']);
        }
        $code = $objCode->generateCode($request['phone']);
        }else{
            return Status::printValidator($curr);
        }
        $arr['result']['code'] = $code;
        return Status::mergeStatus($arr,200);
    }
    
    
    public function ValidateCode(Request $request){
        $arr = array();
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $objCode = new Code();
        $entry = $objCode->where('phone',$request['phone'])->where('code',$request['code'])->where('ttl','>=',Carbon::now())->exists();
        if($entry){
            return Status::printStatus(200);
        }else{
            return Status::mergeStatus($arr,5016,$request['lang_id']);
        }
    }
}
