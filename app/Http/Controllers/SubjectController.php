<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\Status;

class SubjectController extends Controller
{
    public function GetSubjects(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objSubject = new Subject();
        $arrSubjects = $objSubject->get(['id','name'])->translate($lang,'en');
        foreach ($arrSubjects as $obj){
            $obj['id'] = intval($obj['id']);
        }
        $arr['result'] = $arrSubjects;
        return Status::mergeStatus($arr,200);
    }
}
