<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Status;
use App\User;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
    //
    public function Login(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'email'=>'required|email|max:100',
            'password'=> 'required|min:6|max:50',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            if($error->first('password')){
                $arr = Status::mergeStatus($arr,4019,$request['lang_id']);
            }
            if($error->first('email')){
                $arr = Status::mergeStatus($arr,5011,$request['lang_id']);
            }
            return $arr;
            
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
//             $objUser = new User();
//             $obj = $objUser->getUser($user->id);
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            $arr = Status::mergeStatus($arr,401,$request['lang_id']);
            return $arr;
        }

    }
    public function Register(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:30',
            'email'=>'required|email|unique:users|max:100',
            'password'=> 'required|min:6|max:50',
            'phone' =>'required|unique:users|max:15'
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('name')){
                $arr = Status::mergeStatus($arr,4021,$request['lang_id']);
            }
            if($error->first('password')){
                $arr = Status::mergeStatus($arr,4019,$request['lang_id']);
            }
            if($error->first('phone')){
                $arr = Status::mergeStatus($arr,4020,$request['lang_id']);
            }
            if($error->first('email')){
                $arr = Status::mergeStatus($arr,4016,$request['lang_id']);
            }
            return $arr;
            
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['user'] = $user;
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $arr['results'] = $success;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
    
    public function CheckAccount(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'email'=>'email|unique:users|max:100',
            'phone' =>'unique:users|max:15',
            'fb_id'=>'unique:users',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('phone')){
                $arr = Status::mergeStatus($arr,4020,$request['lang_id']);
            }
            if($error->first('email')){
                $arr = Status::mergeStatus($arr,4016,$request['lang_id']);
            }
            if($error->first('fb_id')){
                $arr = Status::mergeStatus($arr,5005,$request['lang_id']);
            }
            return $arr;
        }else{
            $arr = Status::mergeStatus($arr,200);
            return $arr ;
        }
    }
    
    public function Facebook(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'fb_id'=>'required',
            'fb_token'=>'required',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('fb_id')){
                $arr = Status::mergeStatus($arr,4001,$request['lang_id']);
            }
            if($error->first('fb_token')){
                $arr = Status::mergeStatus($arr,4001,$request['lang_id']);
            }
            return $arr;
            
        }
        $objUser = new User();
        if($objUser->where('fb_id', '=', $request['fb_id'])->exists()){
            $user = $objUser->where('fb_id', '=', $request['fb_id'])->first();
            $success['user'] = $user;
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            $ch = curl_init("https://graph.facebook.com/me?fields=id&access_token=".$request['fb_token']);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($output);
            if(!isset($output->error) && $request['fb_id'] == $output->id){
                $input = $request->all();
                $input['password'] = bcrypt($input['password']);
                $user = User::create($input);
                $success['user'] = $user;
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                $arr['results'] = $success;
                $arr = Status::mergeStatus($arr,200);
            }else{
                $arr = Status::mergeStatus($arr,203,$request['lang_id']);
            }
            return $arr;
            
    }
}
    public function GetProfile(Request $request){
        $arr = array();
        $user = Auth::user();
        if(str_contains($user->phone,'+')){
            $user->phone = explode('+', $user->phone)[1];
        }
        $arr['result'] = $user ; 
        return Status::mergeStatus($arr,200);
    }
    
    public function EditProfile(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'name'=>'max:30',
//             'password'=> 'min:6|max:50',
            'phone' =>'max:15'
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            
            if($error->first('name')){
                $arr = Status::mergeStatus($arr,4021,$request['lang_id']);
            }
//             if($error->first('password')){
//                 $arr = Status::mergeStatus($arr,4019,$request['lang_id']);
//             }
            if($error->first('phone')){
                $arr = Status::mergeStatus($arr,4020,$request['lang_id']);
            }
            return $arr;
            
        }
        $objUser = new User();
        $success = $objUser->editProfile($request);
        return $success;
    }
}
