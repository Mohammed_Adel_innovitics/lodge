<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Status;

class ProductController extends Controller
{
    public function GetProductsByOfferId(Request $request){
        $arr = Array();
        $objProduct = new Product();
        $products = $objProduct->getProductsByOfferId($request);
        $arr['result'] = $products;
        
        return Status::mergeStatus($arr,200);
    }
}
