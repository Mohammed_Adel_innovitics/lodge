<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserSearch;
use Illuminate\Support\Facades\Auth;
use App\Status;
use Illuminate\Support\Facades\DB;
use App\Unittype;
use App\Governrate;
use App\Area;
use App\Compound;

class UsersearchController extends Controller
{
    //
    public function GetLastSearch(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $objUserSearch = new UserSearch();
        $search = $objUserSearch->where('user_id',Auth::user()->id)->get()->last();
        if(isset($search) && $search!=null){
        $search = $objUserSearch->where('user_id',Auth::user()->id)->get()->last();
        $objUnit = new Unittype();
        $objGov = new Governrate();
        $objArea = new Area();
        $objComp = new Compound();
        
        $types = json_decode($search['unit_type_id']);
        if(count($types) > 0){
        foreach ($types as $key=>$value){
            $types[$key] = $objUnit->where('id',$types[$key])->first(['id','name'])->translate($request['lang_id'],'en');
        }
        }
        $gov = json_decode($search['governrate_id']);
        if(count($gov) > 0){
        foreach ($gov as $key=>$value){
            $gov[$key] = $objGov->where('id',$gov[$key])->first(['id','name'])->translate($request['lang_id'],'en');
        }
        }
        $areas = json_decode($search['area_id']);
        if(count($areas) > 0){
        foreach ($areas as $key=>$value){
            $areas[$key] = $objArea->where('id',$areas[$key])->first(['id','name'])->translate($request['lang_id'],'en');
        }
        }
        $arrComp = json_decode($search['compound_id']);
        if(count($arrComp) > 0){
        foreach ($arrComp as $key=>$value){
            $arrComp[$key] = $objComp->where('id',$arrComp[$key])->first(['id','name'])->translate($request['lang_id'],'en');
        }
        }
        $search['unit_type_id'] = $types;
        $search['governrate_id']= $gov;
        $search['area_id']= $areas;
        $search['compound_id']= $arrComp;
        $arr['result'] = $search;
        return Status::mergeStatus($arr,200);
        }else{
            $arr['result'] = new \stdClass ; 
            return Status::mergeStatus($arr,200);
        }
    }
}
