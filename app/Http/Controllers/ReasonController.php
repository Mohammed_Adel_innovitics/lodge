<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reason;
use App\Status;

class ReasonController extends Controller
{
    //
    public function ListReasons(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objReason = new Reason();
        $arrReasons = $objReason->get(['id','name'])->translate($lang,'en')->toArray();
        foreach ($arrReasons as $obj){
            $obj['id'] = intval($obj['id']);
        }
        $arr['result'] = $arrReasons;
        return Status::mergeStatus($arr,200);
    }
}
