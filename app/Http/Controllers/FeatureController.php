<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feature;
use App\Option;
use App\Status;
use App\MediaUrl;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\Lodgeoption;
class FeatureController extends VoyagerBaseController
{
    //
    use BreadRelationshipParser;
    
    public function ListFeatures(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objFeature = new Feature();
        $arrFeatures = $objFeature->listFeatures($lang);
        foreach ($arrFeatures as $index=>$feature){
            $feature['id'] = intval($feature['id']);
            $arrFeatures[$index]['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$arrFeatures[$index]['icon']);
            $objOption = new Option();
            $arrFeatures[$index]['options'] = $objOption->getOptionByFeatId($feature['id'],$lang); 
            foreach ($arrFeatures[$index]['options'] as $obj){
                $obj['id'] = intval($obj['id']);
            }
        }
        $arr['result'] = $arrFeatures ; 
        $arr = Status::mergeStatus($arr, 200);
        return $arr ; 
    }
    public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $dataTypeOp = Voyager::model('DataType')->where('slug', '=', 'options')->first();
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;
        
        $dataTypeContentOp = (strlen($dataTypeOp->model_name) != 0)
        ? new $dataTypeOp->model_name()
        : false;
        
        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }
        foreach ($dataTypeOp->addRows as $key => $row) {
            $dataTypeOp->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');
        
        $this->removeRelationshipField($dataTypeOp, 'add');
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $isModelTranslatable = is_bread_translatable($dataTypeContentOp);
        
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','dataTypeOp', 'dataTypeContentOp', 'isModelTranslatableOp'));
    }
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        //validate translatable fil
        if(json_decode($request['name_i18n'])->ar == "" || json_decode($request['name_i18n'])->ar == null
            || json_decode($request['option_1_name_i18n'])->ar == "" || json_decode($request['option_1_name_i18n'])->ar == null
            || json_decode($request['option_2_name_i18n'])->ar == "" || json_decode($request['option_2_name_i18n'])->ar == null
            ){
            $data =  [
                'message'    => 'please enter the arabic name',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }else if(json_decode($request['name_i18n'])->en == "" || json_decode($request['name_i18n'])->en== null
            || json_decode($request['option_1_name_i18n'])->en== "" || json_decode($request['option_1_name_i18n'])->en== null
            || json_decode($request['option_2_name_i18n'])->en== "" || json_decode($request['option_2_name_i18n'])->en== null
            ){
            $data =  [
                'message'    => 'please enter the english name',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        $objOption = new Option();
        $saveOptions = $objOption->addOptionAp($request,$data['id']);
        event(new BreadDataAdded($dataType, $data));
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
            ]);
    }
    public function edit(Request $request, $id)
    {
        $objOption = new Option();
        $arrOptions = $objOption->where('feature_id',$id)->get()->toArray();
//         dd($arrOptions[0]['id']);
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }
        
        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');
        
        // Check permission
        $this->authorize('edit', $dataTypeContent);
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
//         ////option1        
        $dataTypeOp = Voyager::model('DataType')->where('slug', '=', 'options')->first();
        
        if (strlen($dataTypeOp->model_name) != 0) {
            $modelOp = app($dataTypeOp->model_name);
            
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($modelOp && in_array(SoftDeletes::class, class_uses($modelOp))) {
                $modelOp= $modelOp->withTrashed();
            }
            if ($dataTypeOp->scope && $dataTypeOp->scope != '' && method_exists($modelOp, 'scope'.ucfirst($dataTypeOp->scope))) {
                $modelOp= $modelOp->{$dataTypeOp->scope}();
            }
            $dataTypeContentOp = call_user_func([$modelOp, 'findOrFail'], $arrOptions[0]['id']);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContentOp = DB::table($dataTypeOp->name)->where('id', $arrOptions[0]['id'])->first();
        }
        
        foreach ($dataTypeOp->editRows as $key => $row) {
            $dataTypeOp->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }
        
//         // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataTypeOp, 'edit');
        
//         // Check if BREAD is Translatable
        $isModelTranslatableOp = is_bread_translatable($dataTypeContentOp);
//         ////option1

        
//         ////option2
     if (strlen($dataTypeOp->model_name) != 0) {
            $modelOp = app($dataTypeOp->model_name);
            $dataTypeContentOp2 = call_user_func([$modelOp, 'findOrFail'], $arrOptions[1]['id']);
        } else {
            $dataTypeContentOp2 = DB::table($dataTypeOp->name)->where('id', $arrOptions[1]['id'])->first();
        }
//         foreach ($dataTypeOp->editRows as $key => $row) {
//             $dataTypeOp->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
//         }      
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
//         dd($dataTypeContent->getKey());
//         ///
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','dataTypeOp', 'dataTypeContentOp', 'isModelTranslatableOp','dataTypeContentOp2'));
    }
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        
        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }
        
        // Check permission
        $this->authorize('edit', $data);
        
        //validate translatable fil
        if(json_decode($request['name_i18n'])->ar == "" || json_decode($request['name_i18n'])->ar == null
            || json_decode($request['option_1_name_i18n'])->ar == "" || json_decode($request['option_1_name_i18n'])->ar == null
            || json_decode($request['option_2_name_i18n'])->ar == "" || json_decode($request['option_2_name_i18n'])->ar == null
            ){
                $data =  [
                    'message'    => 'please enter the arabic name',
                    'alert-type' => 'error',
                ];
                return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }else if(json_decode($request['name_i18n'])->en == "" || json_decode($request['name_i18n'])->en== null
            || json_decode($request['option_1_name_i18n'])->en== "" || json_decode($request['option_1_name_i18n'])->en== null
            || json_decode($request['option_2_name_i18n'])->en== "" || json_decode($request['option_2_name_i18n'])->en== null
            ){
                $data =  [
                    'message'    => 'please enter the english name',
                    'alert-type' => 'error',
                ];
                return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        $objOption = new Option();
        $arrOptions = $objOption->where('feature_id',$id)->get()->toArray();
        $updateOptions = $objOption->updateOptionAp($request, $id,$arrOptions);
        event(new BreadDataUpdated($dataType, $data));
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
            ]);
    }
    
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        //         dd('lll');
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('delete', app($dataType->model_name));
        
        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            
            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses($model)))) {
                $this->cleanup($dataType, $data);
            }
        }
        
        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
        
        //check if used in any table
        $objOptions = new Option();
        $arrOptionIds = $objOptions->whereIn('feature_id',$ids)->get()->toArray();
        $objLodgeOp = new Lodgeoption();
        $findLodge= $objLodgeOp->whereIn('option_id',$arrOptionIds)->exists();
        if ($findLodge == true) {
            $data =  [
                'message'    => 'Cannot delete this feature because it is assigned to a lodge',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }
        
        $res = $data->destroy($ids);
        $data = $res
        ? [
            'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
            'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
                ];
            
            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }
            
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
}
