<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use Validator;
use App\Complain;

class ComplainController extends Controller
{
    public function SubmitComplain(Request $request){
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'subject_id'=>'required',
            'feedback'=> 'required',
        ]);
        if ($validator->fails()) {
            
            $error = $validator->errors();
            if($error->first('subject_id')){
                $arr = Status::mergeStatus($arr,5012,$request['lang_id']);
            }
            if($error->first('feedback')){
                $arr = Status::mergeStatus($arr,5013,$request['lang_id']);
            }
            return $arr;
            
        }
        $objComplain = new Complain();
        $response = $objComplain->submitComplain($request);
        return Status::mergeStatus($arr,200);
        
    }
}
