<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Intro;
use App\MediaUrl;
use App\Status;

class IntroController extends Controller
{
    //
    public function ListIntro(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objIntro = new Intro();
        $arrofIntros = $objIntro->get(['id','text','img'])->translate($lang,'en')->toArray();
        foreach ($arrofIntros as $index => $obj){
            $obj['id'] = intval($obj['id']);
            $arrofIntros[$index]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$arrofIntros[$index]['img']);
        }
        $arr['result'] = $arrofIntros ; 
        return Status::mergeStatus($arr,200);
    }
}
