<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lodge;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;
use App\Status;
use Exception;
use Illuminate\Support\Facades\DB;
use App\twilio;

class twilioController extends Controller
{
    //
    public function CreateChannel(Request $request){
        $sid    = env('twilio_sid');
        $token  = env('twilio_token');
        $service = env('twilio_service');
        $arr = array();
        $objLodge = new Lodge();
        $user_id = Auth::user()->id ; 
        $lodge = $objLodge->where('ref_code',$request['ref_code'])->first();
        $lodger_id = $lodge['user_id'];
        $lodge_title = $lodge['title'];
        $twilio = new Client($sid, $token);
        try {
            $channel = $twilio->chat->v2->services($service)
        ->channels
        ->create(['type'=>'private','uniqueName'=>$user_id.'-'.$lodger_id.'-'.$request['ref_code'],'friendlyName'=>$lodge_title]);
        }catch (Exception $e){
            $channel = $twilio->chat->v2->services($service)
            ->channels($user_id.'-'.$lodger_id.'-'.$request['ref_code'])
            ->fetch();
            $arr['result']['lodger_name'] = DB::table('users')->where('id',$lodger_id)->first()->name;
            $arr['result']['friendly_name'] = $channel->friendlyName;
            $arr['result']['unique_name'] = $channel->uniqueName;
            $arr['result']['sid'] = $channel->sid;
            return Status::mergeStatus($arr,200);
        }
        
        $member = $twilio->chat->v2->services($service)
        ->channels($channel->sid)
        ->members
        ->create($user_id);
        
        $member = $twilio->chat->v2->services($service)
        ->channels($channel->sid)
        ->members
        ->create($lodger_id);
        
        
        
        $user = $twilio->chat->v2->services($service)
        ->users($user_id)
        ->update(array(
            "friendlyName" => Auth::user()->name
        )
            );
        
        $user = $twilio->chat->v2->services($service)
        ->users($lodger_id)
        ->update(array(
            "friendlyName" => DB::table('users')->where('id',$lodger_id)->first()->name
        )
            );
        
        //print($user->friendlyName);
        
        $arr['result']['lodger_name'] = DB::table('users')->where('id',$lodger_id)->first()->name;
        $arr['result']['friendly_name'] = $channel->friendlyName;
        $arr['result']['unique_name'] = $channel->uniqueName;
        $arr['result']['sid'] = $channel->sid;
        return Status::mergeStatus($arr,200);
    }
    
    public function DeleteChannel(Request $request){
        $sid    = env('twilio_sid');
        $token  = env('twilio_token');
        $service = env('twilio_service');
        $twilio = new Client($sid, $token);
        $channels = $twilio->chat->v2->services($service)
        ->channels
        ->read(array(), 30);
        foreach ($channels as $record) {
//             dd($record->sid);
            $twilio->chat->v2->services($service)
            ->channels($record->sid)
            ->delete();
//             print($record->sid);
        }
    }
    
    public function GenerateToken(Request $request){
        $arr = array();
        $objTwilio = new twilio();
        $token = $objTwilio->generateToken($request);
        $arr['result']['token'] = $token->token;
        return Status::mergeStatus($arr,200);
    }
    
}
