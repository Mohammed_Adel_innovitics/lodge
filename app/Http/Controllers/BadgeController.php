<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Badge;
use App\Status;
use App\MediaUrl;
use Illuminate\Support\Facades\Auth;

class BadgeController extends Controller
{
    //
    public function ListBadges(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objBadge = new Badge();
        $arrBadges = $objBadge->whereIn('id',['1','2','3'])->get(['id','name','icon_list'])->translate($lang,'en')->toArray();
        foreach ($arrBadges as $index=>$icon){
            $icon['id'] = intval($icon['id']);
            $arrBadges[$index]['icon_list'] = str_replace('\\', '/', MediaUrl::getUrl().$arrBadges[$index]['icon_list']);
        }
        $arr['result'] = $arrBadges ; 
        return Status::mergeStatus($arr,200);
    }
    
    public function GetUserBadges(Request $request){
        $arr = array();
        $objBadges = new Badge();
        $res = $objBadges->getBadgeByUser(Auth::user()->id);
        $arr['result'] = $res ;
        return Status::mergeStatus($arr,200);
    }
}
