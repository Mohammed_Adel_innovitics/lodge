<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Status;
use App\User;
use App\PasswordReset;
use Mail;
use Carbon\Carbon;
class PasswordResetController extends Controller
{
    //
    public function Create(Request $request)
    {
        (!isset($request['lang_id'])) ? $request['lang_id'] = 'en' : $request['lang_id'];
        $arr = array();
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);
        if($validator->fails()){
            $arr = Status::mergeStatus($arr,4030,$request['lang_id']);
            return $arr;
        }else{
            $objuser = new User();
            $user = $objuser->where('email','=',$request->email)->first();
            if(!$user){
                $arr = Status::mergeStatus($arr,4030,$request['lang_id']);
                return $arr;
            }
            $passwordReset = PasswordReset::updateOrCreate(['email' => $user->email],['email' => $user->email,'token' => str_random(60)]);
            $localurl = 'http://localhost/lodge/public/api/password/find/'.$passwordReset->token;
            $produrl = 'http://lodge.innsandbox.com/api/password/find/'.$passwordReset->token;
            $uemail = $user->email;
            $username = $user->name;
            $data = array('name'=>"$user->name",'action_url'=>"$produrl",'support_url'=>"http://www.google.com");
            Mail::send('resetPassword', $data, function($message) use ($uemail,$username) {
                $message->to($uemail, $username)->subject
                ('resetting password Lodge Security');
                $message->from('lodge@innsandbox.com','Lodge team');
            }); 
                $arr = Status::mergeStatus($arr,200);
                return $arr;           
        }
    }
    
    public function Find($token)
    {
        $arr = array();
        $passwordReset = PasswordReset::where('token', $token)
        ->first();
        if (!$passwordReset){
            $arr = Status::printStatus(5015);
            return $arr;
        }
            if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $data = array ('result'=>"0");
                return view('updatePassword',$data);
            }
            $data = array ('usermail'=>"$passwordReset->email",'result'=>"1");
            return view('updatePassword',$data);
    }
    
    public function reset(Request $request)
    {
        $request->validate([
            'uemail' => 'required|string|email',
            'password' => 'required|string',
        ]);
            $user = User::where('email', $request->uemail)->first();
            if (!$user){
                $data = array ('result'=>"0");
                return view('updatePassword',$data);
            }
            $userupdate = User::updateOrCreate(['email' => $user->email],['password' => bcrypt($request->password)]);
        $passwordReset = PasswordReset::where('email',$user->email)->delete();
        $data = array ('result'=>"2");
        return view('updatePassword',$data);
    }
}
