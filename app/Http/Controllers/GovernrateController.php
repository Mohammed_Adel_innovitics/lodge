<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Governrate;
use App\Status;
use Validator;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\Area;
use App\Lodge;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;

class GovernrateController extends VoyagerBaseController
{
    //
    public function ListGovernrates(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objGov = new Governrate();
        $arrGov = $objGov->join('areas','governrates.id','areas.governrate_id')->where('areas.deleted_at','=',null)->OrderBy('name','ASC')->distinct('governrates.id')->get(['governrates.id','governrates.name'])->translate($lang,'en')->toArray();
        foreach ($arrGov as $obj){
            $obj['id'] = intval($obj['id']);
        }
        $arr['result'] = $arrGov;
        $arr = Status::mergeStatus($arr, 200);
        return $arr;
    }
    
    public function GetCoordinatesByAdd(Request $request){
        $ref_code = 
        $arr = array();
        $res= array();
        $apiKey  = 'AIzaSyDi8jdu9wJRQ6Qdn_c0xHNbFRZrNRHDCOw';
        $address = urlencode($request['address']);
        $url     = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$apiKey}";
        $resp    = json_decode( file_get_contents( $url ), true );
        $resp['results'][0]['geometry']['location']['lat'] = 30.0426645;
        $resp['results'][0]['geometry']['location']['lng'] = 31.4682178;
        $lat    = $resp['results'][0]['geometry']['location']['lat'];
        $long   = $resp['results'][0]['geometry']['location']['lng'];
        $res['coordinates']['lat'] = $lat;
        $res['coordinates']['lng'] = $long;
        $res['ref_code'] = $this->generateCode();
        $arr['result'] = $res;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
    
    public function generateCode(){
            
            $ref_code = str_random(6);
            
            $validator = Validator::make(['ref_code'=>$ref_code],['ref_code'=>'unique:lodges']);
            
            if($validator->fails()){
                return $this->generateCode();
            }
            
            return $ref_code;
        }
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);
//         dd('lll');
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('delete', app($dataType->model_name));
        
        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            
            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses($model)))) {
                $this->cleanup($dataType, $data);
            }
        }
        
        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
        
            //check if used in any table
            $objArea = new Area();
            $objLodge = new Lodge();
            $findArea = $objArea->whereIn('governrate_id',$ids)->exists();
            $findLodge= $objLodge->whereIn('governrate_id',$ids)->exists();
            if ($findArea == true || $findLodge == true) {
                $data =  [
                    'message'    => 'Cannot delete this governrate because it is assigned to a lodge or area',
                    'alert-type' => 'error',
                ];
                return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
            }
        
        $res = $data->destroy($ids);
        $data = $res
        ? [
            'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
            'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
                ];
            
            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }
            
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
    public function store(Request $request)
    {
        
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        //validate translatable fil
        if(json_decode($request['name_i18n'])->ar == "" || json_decode($request['name_i18n'])->ar == null){
            $data =  [
                'message'    => 'please enter the arabic name',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }else if(json_decode($request['name_i18n'])->en == "" || json_decode($request['name_i18n'])->en == null){
            $data =  [
                'message'    => 'please enter the english name',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        
        event(new BreadDataAdded($dataType, $data));
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
            ]);
    }
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        
        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }
        
        // Check permission
        $this->authorize('edit', $data);
        
        //validate translatable fil
        if(json_decode($request['name_i18n'])->ar == "" || json_decode($request['name_i18n'])->ar == null){
            $data =  [
                'message'    => 'please enter the arabic name',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }else if(json_decode($request['name_i18n'])->en == "" || json_decode($request['name_i18n'])->en == null){
            $data =  [
                'message'    => 'please enter the english name',
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        
        event(new BreadDataUpdated($dataType, $data));
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
            ]);
    }
}
