<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Status;

class PackageController extends Controller
{
    //
    public function ListPackages(Request $request){
        $lang = $request['lang_id'];
        $arr = array();
        $objPackage = new Package();
        $arrPackages = $objPackage->get(['id','days','price','title','desc','is_featured','price_visible'])->translate($lang,'en')->toArray();
        foreach ($arrPackages as $obj){
            $obj['id'] = intval($obj['id']);
        }
        $arr['result'] = $arrPackages;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
}
