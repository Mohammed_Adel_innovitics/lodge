<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apntoken;
use Illuminate\Support\Facades\Auth;
use App\Status;


class ApntokenController extends Controller
{
    //
    public function AddApn(Request $request){
        if(Auth::user() == null){
            $arr = array();
            $objApn = new Apntoken();
            if($objApn->where('token',$request['token'])->exists()){
                return Status::printStatus(5014);
            }else{
                $objApn->user_id = null;
                $objApn->token = $request['token'];
                $objApn->save();
                return Status::printStatus(200);
            }
        }else{
        $arr = array();
        $objApn = new Apntoken();
        if($objApn->where('user_id',Auth::user()->id)->where('token',$request['token'])->exists()){
            return Status::printStatus(5014);
        }else{
        $objApn->user_id = Auth::user()->id;
        $objApn->token = $request['token'];
        $objApn->save();
        return Status::printStatus(200);
        }
        }
    }
    
    public function DeleteApn(Request $request){
        $arr = array();
        $objApn = new Apntoken();
        $res = $objApn->where('token',$request['token'])->delete();
        return Status::printStatus(200);
    }
    
    public function SendNotifi(Request $request){
        $arr = array();
        $objApn = new Apntoken();
        $res = $objApn->sendNotifi($request['user_id'],$request['title'],$request['content']);
        return Status::printStatus(200);
    }
    
}
