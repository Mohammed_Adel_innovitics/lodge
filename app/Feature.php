<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model
{
    use Translatable;
    use SoftDeletes;
    protected $fillable = [
        'id', 'name', 'icon'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name'];
    
    public function listFeatures($lang = null){
        $arrFeatures = $this->get(['id','name','icon'])->translate($lang,'en')->toArray();
        return $arrFeatures ; 
    }
}
