<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Badge extends Model
{
    use SoftDeletes;
    use Translatable;
    protected $fillable = [
        'id', 'name', 'icon','icon_list'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name'];
    
    public function getBadgeByUser($user_id,$lang=null){
        $arrBadges = array();
        $objRate = new Rate();
        $objRateBadges = new RatesBadge();
        $arrRateIds = $objRate->where('owner_id',$user_id)->pluck('id');
//         dd($arrRateIds);
//         $Badges = DB::select('SELECT count(*) as count,AVG(value) as avg,badge_id from rates_badges WHERE In rate_id ='.$arrRateIds.' GROUP BY badge_id ');
        $Badges = DB::table('rates_badges')->whereIn('rate_id',$arrRateIds)->selectRaw('COUNT(*) as count ,AVG(value) as avg,badge_id')->groupBy('badge_id')->get();
//         dd($Badges);
        foreach ($Badges as $obj){
             $res = $this->checkBadges($obj->badge_id,$obj->count,$obj->avg,$lang);
             ($res == null ) ? null : $arrBadges[] = $res;
        }
        if(DB::table('lodges')->where('user_id',$user_id)->whereIn('status_id',['2','3'])->count() >= 10){
            $bdg = $this->where('id','4')->first(['id','name','icon'])->translate($lang,'en');
            $bdg['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$bdg['icon']);
            $arrBadges [] = $bdg;
        }
//         dd(count($arrBadges));
        if(count($arrBadges) == 4){
            $bdg = $this->where('id','5')->first(['id','name','icon'])->translate($lang,'en');
            $bdg['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$bdg['icon']);
            $arrBadges [] = $bdg;
        }
        return $arrBadges;
//         dd($arrBadges);
    }
    
    public function checkBadges($badge_id,$count=null,$avg=null,$lang=null){
        switch ($badge_id){
            case '1':
                $badge = $this->where('id',$badge_id)->first(['id','name','icon'])->translate($lang,'en');
                if($count >= 5 && $avg > 2.5){
                    $badge['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$badge['icon']);
                    $arr = $badge;
                    return $arr;
                }
            break;
            
            case '2':
                $badge = $this->where('id',$badge_id)->first(['id','name','icon'])->translate($lang,'en');
                if($count >= 5 && $avg > 2.5){
                    $badge['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$badge['icon']);
                    $arr = $badge;
                    return $arr;
                    
                }
            break;
            
            case '3':
                $badge = $this->where('id',$badge_id)->first(['id','name','icon'])->translate($lang,'en');
                if($count >= 5 && $avg > 2.5){
                    $badge['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$badge['icon']);
                    $arr = $badge;
                    return $arr;
                }
            break;
            default:
                return  null;
        }
    }
}
