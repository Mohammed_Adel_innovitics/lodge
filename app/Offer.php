<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use Carbon\Carbon;


class Offer extends Model
{
    use Translatable,SoftDeletes;
    
    protected $fillable = [
        'id', 'title','img', 'desc', 'storecategories_id', 'exp_date', 'type'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['title', 'type', 'desc'];
    
    public function getOffersByCatId($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
                
        $objOffer =  $this->newQuery();
        
        $objOffer->join('store_categories', 'offers.storecat_id', 'store_categories.id')
        ->where('store_categories.cat_id', $request['cat_id'])
        ->join('stores', 'store_categories.store_id', 'stores.id');
        
        //
        if($lang == 'ar'){
            $objOffer->select('offers.id', 'offers.title','offers.img', 'offers.type', 'translations.value as store_name')
            ->join('translations', 'stores.id', 'translations.foreign_key')
            ->where('locale', $lang)->where('table_name', 'stores')->where('column_name', 'name');
        }else{
            $objOffer->select('offers.id', 'offers.title','offers.img', 'offers.type','stores.name as store_name');
        }
        
        $arrOffers = $objOffer->whereDate('exp_date', '>', Carbon::now())->get()->translate($lang,'en');
        
        foreach ($arrOffers as $index=>$obj){
            $arrOffers[$index]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$arrOffers[$index]['img']);
        }
        
        return $arrOffers;
    }
    
    public function getOffersByStoreId($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
                      
        $arrOffers = $this->select('offers.id', 'offers.title', 'offers.img', 'offers.type')
        ->join('store_categories', 'offers.storecat_id', 'store_categories.id')
        ->where('store_categories.store_id', $request['store_id'])
        ->whereDate('exp_date', '>', Carbon::now())->get()->translate($lang,'en');;
        
        foreach ($arrOffers as $index=>$obj){
            $arrOffers[$index]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$arrOffers[$index]['img']);
        }
        
        return $arrOffers;
    }
    
    public function getOfferById($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $offer = $this->select('id', 'title', 'img', 'type', 'desc')
        ->where('id', $request['offer_id'])->get()->translate($lang,'en');
        
        $offer[0]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$offer[0]['img']);
       
        return $offer;
    }
}
