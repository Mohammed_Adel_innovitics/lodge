<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Complain extends Model
{
    protected $fillable = [
        'id', 'subject_id', 'ref_code','feedback'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
        public function submitComplain($request){
            $this->subject_id = $request['subject_id'];
            $this->ref_code = $request['ref_code'];
            $this->feedback = $request['feedback'];
            $this->user_id = Auth::user()->id;
            $this->save();
            return $this;
        }
}
