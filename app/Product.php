<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use App\Helpers\translationHelper;


class Product extends Model
{
    use Translatable, SoftDeletes;
    
    protected $fillable = [
        'id', 'name','img', 'original_price', 'discounted_price', 'offer_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name'];
    
    public function getProductsByOfferId($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $arrProducts = $this->where('offer_id', $request['offer_id'])->paginate(5);
        
        $arrProductsTrans = $arrProducts->translate($lang,'en');
        
        foreach ($arrProductsTrans as $index=>$obj){
            $arrProductsTrans[$index]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$arrProductsTrans[$index]['img']);
        }
        
        $arrProd = translationHelper::translatedCollectionToArray($arrProductsTrans);
        $arrProd = translationHelper::paginationForTranslator($arrProducts, $arrProductsTrans);
               
        return $arrProd;
    }
}
