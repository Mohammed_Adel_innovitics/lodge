<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class StoreCategory extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'id', 'store_id','category_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
}
