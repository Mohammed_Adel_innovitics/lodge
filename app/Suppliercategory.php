<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use App\Helpers\translationHelper;


class Suppliercategory extends Model
{
    use Translatable,SoftDeletes;
    
    protected $fillable = [
        'id', 'name','img'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name'];
    
    public function getSupplierCategories($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $arrSupplierCategories = $this->paginate(5, ['id', 'img', 'name']);
        $arrSupplierCategoriesTrans = $arrSupplierCategories->translate($lang,'en');
        foreach ($arrSupplierCategoriesTrans as $index=>$obj){
            $arrSupplierCategoriesTrans[$index]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$arrSupplierCategoriesTrans[$index]['img']);
        }
        $arrSupp = translationHelper::translatedCollectionToArray($arrSupplierCategoriesTrans);
        $arrSupp = translationHelper::paginationForTranslator($arrSupplierCategories, $arrSupplierCategoriesTrans);
        
        return $arrSupp;
    }
}
