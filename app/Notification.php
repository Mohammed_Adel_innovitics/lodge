<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use LaravelFCM\Facades\FCM;


class Notification extends Model
{
    protected $table = 'notifications';
    
    
    
    protected $fillable = ['id','content','updated_at','created_at','title','topic'];
    
   
    
    public function sendNotificationToTopic($topicval,$title,$content){
        
        //         dd($topicval);
        
        $notificationBuilder = new PayloadNotificationBuilder($title);
        
        $notificationBuilder->setBody($content)
        
        ->setSound('default');
        
        $notification = $notificationBuilder->build();
        
        $topic = new Topics();
        
        $topic->topic($topicval);
        
        $topicResponse = FCM::sendToTopic($topic, null, $notification, null);
        
        $topicResponse->isSuccess();
        
        $topicResponse->shouldRetry();
        
        $topicResponse->error();
        
        return $topicResponse;
        
    }
    
    public function sendNotificationToSpecificUser($user_id,$title,$content){
        
        $objapn= new ApnToken();
        
        $tokenofuser = $objapn->getApnTokenOfUser($user_id);
        
        // dd($tokenofuser);
        
        if(count($tokenofuser)>0){
            
            $notificationBuilder = new PayloadNotificationBuilder($title);
            
            $notificationBuilder->setBody($content)
            
            ->setSound('default');
            
            $notification = $notificationBuilder->build();
            
            $downstreamResponse = FCM::sendTo($tokenofuser, null, $notification, null);
            
            $downstreamResponse->numberSuccess();
            
            $downstreamResponse->numberFailure();
            
            $downstreamResponse->numberModification();
            
            return $downstreamResponse;
            
        }
        
        else
            
            return null;
            
            
            
    }
    
    public function save(array $options = [])
    {            
        $this->sendNotificationToTopic($this->topic,$this->title, $this->content); 
        parent::save();
    }
}
