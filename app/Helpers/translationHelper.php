<?php

namespace App\Helpers;

use TCG\Voyager\Traits\Translatable;


class translationHelper 
{

	static function paginationForTranslator($arrBeforeTranslate,$arrAfterTranslate){
   	
   	   $currentPage = $arrBeforeTranslate->currentPage();
       //$totalPages = $arrBeforeTranslate->Total();
       $itemsCount = $arrBeforeTranslate->count();
       $nextPageUrl = $arrBeforeTranslate->nextPageUrl();
       $lastPage = $arrBeforeTranslate->lastPage();
       //$path = URL::current();

       
       $data = array();
       $data['current_page'] = $currentPage;
       $data['last_page'] = $lastPage;
       $data['items'] = $itemsCount;
       $data['next_url'] = $nextPageUrl;
       $data['data'] = $arrAfterTranslate;
       
       return $data;

   } 

   static function translatedCollectionToArray(\TCG\Voyager\Translator\Collection $translatedCollection)
    {
    	//dd($translatedCollection);
	    $collectionArray = array();
	    foreach($translatedCollection as $collectionElement)
	    {
	        $collectionArray[] = array_map(function($rawAttributeData) { return $rawAttributeData['value']; }, $collectionElement->getRawAttributes());
	    }
	    return $collectionArray;
	}
}