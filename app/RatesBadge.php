<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RatesBadge extends Model
{
    protected $fillable = [
        'id','rate_id','badge_id','value'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
}
