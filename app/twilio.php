<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class twilio extends Model
{
    //
    public function generateToken($request){    
        $headers = array
        (
            'Authorization: Basic U3BvcnRveWE6Q3Jpc3RpYW5vXzIw',
            'Content-Type: application/json',
            'Accept: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://burgundy-duck-6145.twil.io/chat-token?identity='.Auth::user()->id.'&device='.$request['device_id']);
        curl_setopt( $ch,CURLOPT_POST, false );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        $result = curl_exec($ch );
        curl_close( $ch );
        return json_decode($result);
    }
}
