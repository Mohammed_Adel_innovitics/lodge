<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Tip extends Model
{
    use SoftDeletes;
    use Translatable;
    protected $fillable = [
        'sentence'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['sentence'];
    public function getPrice($type){
        $objLodge = new Lodge();
        $obj = $objLodge->newQuery();
        $obj->where('process',$type)
        ->where('status_id','=','2');
        $obj->join('currencies', 'currencies.id', '=', 'lodges.currency_id');
        $res = $obj->selectRaw('(rate*price) as price')->OrderBy('price','ASC')->get()->toArray();
//         dd($res);
        if(count($res)>0){
            $result[$type.'_min_price'] = (int)$res[0]['price'];
            $result[$type.'_max_price'] = (int)$res[count($res)-1]['price'];
        }else{
            $result[$type.'_min_price'] = 0 ;
            $result[$type.'_max_price'] = 0 ;
        }
        return $result ; 
    }
    
    public function getBedrooms($type){
        $objLodge = new Lodge();
        $bedrooms = $objLodge->where('process',$type)->where('status_id','=','2')->selectRaw('max(bedrooms) as max_bedrooms,min(bedrooms) as min_bedrooms')->get()->toArray();
        if(count($bedrooms) > 0 && count($bedrooms[0]['max_bedrooms']) != null && count($bedrooms[0]['min_bedrooms']) != null){
                $result[$type.'_max_bedrooms'] = $bedrooms[0]['max_bedrooms'];
                $result[$type.'_min_bedrooms'] = $bedrooms[0]['min_bedrooms'];
            }else{
                $result[$type.'_max_bedrooms'] = 0 ;
                $result[$type.'_min_bedrooms'] = 0 ;
            }
            return $result;
    }
    
    public function getBathrooms($type){
        $objLodge = new Lodge();
        $bathrooms = $objLodge->where('process',$type)->where('status_id','=','2')->selectRaw('max(bathrooms) as max_bathrooms,min(bathrooms) as min_bathrooms')->get()->toArray();
        if(count($bathrooms) > 0 && count($bathrooms[0]['max_bathrooms']) != null && count($bathrooms[0]['min_bathrooms']) != null){
                $result[$type.'_max_bathrooms'] = $bathrooms[0]['max_bathrooms'];
                $result[$type.'_min_bathrooms'] = $bathrooms[0]['min_bathrooms'];
            }else{
                $result[$type.'_max_bathrooms'] = 0 ;
                $result[$type.'_min_bathrooms'] = 0 ;
            }
            return $result;
    }
    
    public function getSize($type){
        $objLodge = new Lodge();
        $size = $objLodge->where('process',$type)->where('status_id','=','2')->selectRaw('max(size) as max_size,min(size) as min_size')->get()->toArray();
        if(count($size) > 0 && count($size[0]['max_size']) != null && count($size[0]['min_size']) != null){
                $result[$type.'_max_size'] = $size[0]['max_size'];
                $result[$type.'_min_size'] = $size[0]['min_size'];
            }else{
                $result[$type.'_max_size'] = 0 ;
                $result[$type.'_min_size'] = 0 ;
            }
            return $result;
    }
}
