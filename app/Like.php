<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Like extends Model
{
    protected $fillable = [
        'id', 'lodg_id','user_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    public function isLike($lodgeId,$userId){
        $entry = $this->where('lodge_id',$lodgeId)->where('user_id',$userId)->get()->toArray();
        return (count($entry)>0) ? $entry[0]['value'] : '0' ; 
    }

    public function getFavLodges($userId){
        $arrLodgeIds = $this->where('user_id',$userId)->where('value',1)->pluck('lodge_id');
        return $arrLodgeIds ; 
    }
}
