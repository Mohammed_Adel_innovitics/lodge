<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Period extends Model
{
    use Translatable;
    use SoftDeletes;
    
    protected $fillable = [
        'id', 'name', 'eq_days'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name'];
}
