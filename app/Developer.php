<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;


class Developer extends Model
{
    use Translatable,SoftDeletes,Spatial;
    
    protected $fillable = [
        'id', 'areas','img', 'logo', 'desc', 'user_id', 'website', 'youtube', 'facebook', 'instagram', 'coordinates'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['desc'];
    protected $spatial = ['coordinates'];
    
    public function getDevelopers($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $arrDevelopers = $this->select('developers.id', 'developers.areas', 'developers.logo', 'developers.user_id', 'users.name')
        ->join('users', 'developers.user_id', 'users.id')->paginate(5, ['id', 'areas', 'logo', 'name']);
        $arrDevelopersTrans = $arrDevelopers->translate($lang,'en');
        
        foreach ($arrDevelopersTrans as $index=>$obj){
            $arrDevelopersTrans[$index]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$arrDevelopersTrans[$index]['img']);
            $arrDevelopersTrans[$index]['logo'] = str_replace('\\', '/', MediaUrl::getUrl().$arrDevelopersTrans[$index]['logo']);
        }
        
        $arrDev = translationHelper::translatedCollectionToArray($arrDevelopersTrans);
        $arrDev = translationHelper::paginationForTranslator($arrDevelopers, $arrDevelopersTrans);
        
        return $arrDev;
    }
    
    public function getDeveloperById($request){
        (isset($request['lang_id'])) ? $lang = $request['lang_id'] : $lang = 'en';
        
        $developer = $this->select('developers.*', 'users.name')
        ->join('users', 'developers.user_id', 'users.id')->where('developers.id', $request['dev_id'])->get()->translate($lang,'en');

        $developer[0]['img'] = str_replace('\\', '/', MediaUrl::getUrl().$developer[0]['img']);
        $developer[0]['coordinates'] = $this->where('id', $request['dev_id'])->get()[0]->getCoordinates();
        return $developer;
    }
}
