<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Option extends Model
{
    use Translatable;
    use SoftDeletes;
    protected $fillable = [
        'id', 'name', 'feature_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    protected $translatable = ['name'];
    
    public function getOptionByFeatId($feature_id,$lang = null){
        $arrOptions = $this->where('feature_id',$feature_id)->get(['id','name','feature_id'])->translate($lang,'en')->toArray();
        return $arrOptions;
    }
    public function addOptionAp($request,$featureId){
        $arrOp = array();
        $arrOp['options'][] = json_decode($request['option_1_name_i18n']);
        $arrOp['options'][] = json_decode($request['option_2_name_i18n']);
//         dd($arrOp);
        foreach ($arrOp['options'] as $obj){
            $const = new Option();
            $const->feature_id = $featureId;
            $const->name = $obj->en;
            $const->save();
            DB::table('translations')->insert(['table_name'=>'options','column_name'=>'name','foreign_key'=>$const->id,'locale'=>'ar','value'=>$obj->ar]);
        }
    }
    public function updateOptionAp($request,$featureId,$arrOptions){
        $arrOp = array();
        $arrOp['options'][] = json_decode($request['option_1_name_i18n']);
        $arrOp['options'][] = json_decode($request['option_2_name_i18n']);
        //         dd($arrOp);
        foreach ($arrOp['options'] as $key=>$obj){
            $const = new Option();
            $const->where('id',$arrOptions[$key]['id'])->update(['name'=>$obj->en]);
            DB::table('translations')->where('foreign_key',$arrOptions[$key]['id'])->update(['value'=>$obj->ar]);
        }
    }
}
