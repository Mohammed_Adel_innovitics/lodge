<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Traits\Spatial;
use Carbon\Carbon;
use function GuzzleHttp\json_decode;


class Lodge extends Model
{
    use Spatial;
    protected $table = 'lodges';
    protected $spatial = ['coordinates'];
    protected $fillable = [
        'id', 'process','unit_type_id','bedrooms','bathrooms','size','floor_no','price',
        'currency_id','period_id','governrate_id','area_id','compound_id','title','desc','img','floor_plan',
        'ref_code','status_id','count_views','count_like','expiry_date','notes','created_at'
    ];
    
    protected $hidden = [
         'deleted_at','updated_at'
    ];
    
    
    
    
    public function addLodge($request){
        $this->process = $request['process'];
        $this->unit_type_id = $request['unit_type_id'];
        $this->bedrooms = $request['bedrooms'];
        $this->bathrooms = $request['bathrooms'];
        $this->size = $request['size'];
        $this->floor_no = $request['floor_no'];
        $this->price = $request['price'];
        $this->currency_id = $request['currency_id'];
        $this->period_id = $request['period_id'];
        $this->governrate_id = $request['governrate_id'];
        $this->area_id = $request['area_id'];
        $this->compound_id = $request['compound_id'];
        $this->title = $request['title'];
        $this->desc = $request['desc'];
        $this->coordinates = (DB::raw('ST_GeomFromText(\'POINT('.$request['lat'].' '.$request['lng'].')\')'));
        $this->ref_code = $request['ref_code'];
        $this->status_id = 1 ;
        $this->user_id = Auth::user()->id ;
        $this->img = $request['gallery'];
        $this->floor_plan = $request['floor'];
        $this->iframe = "<iframe
          width="."100%"."
          height="."100%"."
          frameborder=0"."
          scrolling=yes"."
          marginheight=0"."
          marginwidth=0"."
          src="."https://maps.google.com/maps?q=".$request['lat'].",".$request['lng']."&hl=es;z=1&amp;output=embed"."></iframe>";
        $result = $this->save();
//         $objApn = new Apntoken();
//         $notifi = $objApn->sendNotifi(Auth::user()->id,'Lodge','Your Lodge #'.$request['ref_code'].' is In Review and we will notifi you once it is approved');
        return  $this ; 
    }
    
    public function save(array $options = [])
    
    {
        $currentLodge = DB::table('lodges')->where('id',$this->id)->first();
        if($currentLodge != null){
        if($this->status_id != $currentLodge->status_id && $this->status_id == 2){
            $objpackageTrans = new Packagetransaction();
            $transInfo = $objpackageTrans->getPackageByLodge($this->id);
            if($transInfo['status_id'] == 2){
                $lastInReview = $objpackageTrans->getLastInReview($this->id);
                $remDays = Carbon::parse($transInfo['end'])->diffInDays($lastInReview['created_at']);
                $add = $objpackageTrans->addTransaction($transInfo->package_id,$this->id,2,Carbon::now()->toDateString(),Carbon::now()->addDays($remDays)->toDateString(),0,null,null);
            }else{
            $add = $objpackageTrans->addTransaction($transInfo->package_id,$this->id,2,Carbon::now()->toDateString(),Carbon::now()->addDays(60)->toDateString(),0,null,null);
            }
        }
        }
        $status = DB::table('statuses')->where('id',$this->status_id)->first();
        $objApn = new Apntoken();
        $notifi = $objApn->sendNotifi($this->user_id,'Lodge','Your Lodge #'.$this->ref_code.' is '.$status->name);
        parent::save();
        
    }
    
    public function listLatest($input){
//         $userId = Auth::user()->id ;
        if((array_key_exists('limit',$input)) && $input['limit']!= "" && $input['limit']!=null){
            if(Auth::user() != null){
                $arrLodges = $this->where('process',$input['type'])->where('status_id',2)->OrderBy('lodges.id','DESC')
                ->leftjoin('rates',function ($join){
                        $join->on('rates.lodge_id', '=', 'lodges.id')
                        ->where('rates.rater_id',Auth::user()->id);
                    })->select("lodges.*",
                    DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = '.Auth::user()->id.' THEN "1"
                        ELSE "0"
                        END) AS is_rated '))
                        ->take($input['limit'])->get();
            }else{
                $arrLodges = $this->where('process',$input['type'])->where('status_id',2)->OrderBy('id','DESC')
                ->leftjoin('rates',function ($join){
                    $join->on('rates.lodge_id', '=', 'lodges.id')
                    ->where('rates.rater_id','0');
                })->select("lodges.*",
                    DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = 0 THEN "1"
                        ELSE "0"
                        END) AS is_rated '))
                        ->take($input['limit'])->get();
            }
            
        }else{
            $query = $this->newQuery();
            if((array_key_exists('max_bedrooms',$input)) && ($input['max_bedrooms']) != null && ($input['max_bedrooms']) != ""){
                $query->where('bedrooms','<=',(int)$input['max_bedrooms']);
            }
            if((array_key_exists('min_bedrooms',$input)) && ($input['min_bedrooms']) != null && ($input['min_bedrooms']) != ""){
                $query->where('bedrooms','>=',(int)$input['min_bedrooms']);
            }
            if((array_key_exists('max_bathrooms',$input)) && ($input['max_bathrooms']) != null && ($input['max_bathrooms']) != ""){
                $query->where('bathrooms','<=',(int)$input['max_bathrooms']);
            }
            if((array_key_exists('min_bathrooms',$input)) && ($input['min_bathrooms']) != null && ($input['min_bathrooms']) != ""){
                $query->where('bathrooms','>=',(int)$input['min_bathrooms']);
            }
            if((array_key_exists('max_size',$input)) && ($input['max_size']) != null && ($input['max_size']) != ""){
                $query->where('size','<=',(int)$input['max_size']);
            }
            if((array_key_exists('min_size',$input)) && ($input['min_size']) != null && ($input['min_size']) != ""){
                $query->where('size','>=',(int)$input['min_size']);
            }
            if((array_key_exists('list_order',$input)) && ($input['list_order']) != null && ($input['list_order']) != ""){
                $query->OrderBy('id',$input['list_order']);
            }else{
                $query->OrderBy('id','DESC');
            }
            if((array_key_exists('price_order',$input)) && ($input['price_order']) != null && ($input['price_order']) != ""){
                $query->join('currencies', 'currencies.id', '=', 'lodges.currency_id')->selectRaw('lodges.*,lodges.id,(currencies.rate*lodges.price) as acc_price')->OrderBy('acc_price',$input['price_order'])
                ->groupBy(
                    'lodges.id','process','unit_type_id','bedrooms','bathrooms','size','floor_no','price',
                    'currency_id','period_id','governrate_id','area_id','compound_id','title','desc','img','floor_plan',
                    'ref_code','status_id','count_views','count_like','expiry_date','notes','lodges.created_at','coordinates','lodges.updated_at','lodges.deleted_at',
                    'lodges.user_id','lodges.iframe','currencies.rate'
                    );
            }
            if((array_key_exists('rate_order',$input)) && ($input['rate_order']) != null && ($input['rate_order']) != ""){
                $query->join('rates', 'rates.owner_id', '=', 'lodges.user_id')->selectRaw('lodges.*,lodges.id,avg(rates.overall) as total_rate')->OrderBy('total_rate',$input['rate_order'])
                ->groupBy(
                    'lodges.id','process','unit_type_id','bedrooms','bathrooms','size','floor_no','price',
                    'currency_id','period_id','governrate_id','area_id','compound_id','title','desc','img','floor_plan',
                    'ref_code','status_id','count_views','count_like','expiry_date','notes','lodges.created_at','coordinates','lodges.updated_at','lodges.deleted_at',
                    'lodges.user_id','lodges.iframe'
                    );
            }
            if(Auth::user() != null){
                $query->leftjoin('rates',function ($join){
                    $join->on('rates.lodge_id', '=', 'lodges.id')
                    ->where('rates.rater_id',Auth::user()->id);
                })->select("lodges.*",
                    DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = '.Auth::user()->id.' THEN "1"
                        ELSE "0"
                        END) AS is_rated '));
            }else{
                $query->leftjoin('rates',function ($join){
                    $join->on('rates.lodge_id', '=', 'lodges.id')
                    ->where('rates.rater_id','0');
                })->select("lodges.*",
                    DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = 0 THEN "1"
                        ELSE "0"
                        END) AS is_rated '));
            }
            $arrLodges = $query->where('process',$input['type'])->where('status_id',2)->paginate(5);
        }
        $arr = $this->getLodgeDetails($arrLodges,$input['lang_id']);
        return $arr;
    }
    
    public function listLodgesByUser($userId,$limit,$lang=null){
        if($limit != "" && $limit!=null){
            $arrLodges = $this->where('user_id',$userId)->where('status_id',2)->take($limit)->get();
        }else{
            $arrLodges = $this->where('user_id',$userId)->where('status_id',2)->paginate(5);
        }
        $arr = $this->getLodgeDetails($arrLodges,$lang);
        return $arr;
    }
    
    public function searchLodge($input){
        $lodge = $this->newQuery();
        //         $lodge
        
        if((array_key_exists('process',$input)) && ($input['process']) != null && ($input['process']) != ""){
            $lodge->where('process',$input['process']);
        }
        if((array_key_exists('type',$input)) && ($input['type']) != null && ($input['type']) != ""){
            $lodge->whereIn('unit_type_id',$input['type']);
        }
        if((array_key_exists('gov_id',$input)) && ($input['gov_id']) != null && ($input['gov_id']) != ""){
            $lodge->whereIn('governrate_id',$input['gov_id']);
        }
        if((array_key_exists('area_id',$input)) && ($input['area_id']) != null && ($input['area_id']) != ""){
            $lodge->whereIn('area_id',$input['area_id']);
        }
        if((array_key_exists('compound_id',$input)) && ($input['compound_id']) != null && ($input['compound_id']) != ""){
            $lodge->whereIn('compound_id',$input['compound_id']);
        }
        if((array_key_exists('price_min',$input)) && ($input['price_min']) != null && ($input['price_min']) != "" && (array_key_exists('price_max',$input)) && ($input['price_max']) != null && ($input['price_max']) != ""){
            $lodge->join('currencies as c1', 'c1.id', '=', 'lodges.currency_id')->whereRaw('(c1.rate*lodges.price >='.$input['price_min'].')')->whereRaw('(c1.rate*lodges.price <='.$input['price_max'].')');
            
        }
        if((array_key_exists('keyword',$input)) && ($input['keyword']) != null && ($input['keyword']) != ""){
            $lodge->where('desc','LIKE','%'.$input['keyword'].'%');
        }
        //filter and sort section
        if((array_key_exists('max_bedrooms',$input)) && ($input['max_bedrooms']) != null && ($input['max_bedrooms']) != ""){
            $lodge->where('bedrooms','<=',(int)$input['max_bedrooms']);
        }
        if((array_key_exists('min_bedrooms',$input)) && ($input['min_bedrooms']) != null && ($input['min_bedrooms']) != ""){
            $lodge->where('bedrooms','>=',(int)$input['min_bedrooms']);
        }
        if((array_key_exists('max_bathrooms',$input)) && ($input['max_bathrooms']) != null && ($input['max_bathrooms']) != ""){
            $lodge->where('bathrooms','<=',(int)$input['max_bathrooms']);
        }
        if((array_key_exists('min_bathrooms',$input)) && ($input['min_bathrooms']) != null && ($input['min_bathrooms']) != ""){
            $lodge->where('bathrooms','>=',(int)$input['min_bathrooms']);
        }
        if((array_key_exists('max_size',$input)) && ($input['max_size']) != null && ($input['max_size']) != ""){
            $lodge->where('size','<=',(int)$input['max_size']);
        }
        if((array_key_exists('min_size',$input)) && ($input['min_size']) != null && ($input['min_size']) != ""){
            $lodge->where('size','>=',(int)$input['min_size']);
        }
        if((array_key_exists('list_order',$input)) && ($input['list_order']) != null && ($input['list_order']) != ""){
            $lodge->OrderBy('lodges.id',$input['list_order']);
        }
        if((array_key_exists('price_order',$input)) && ($input['price_order']) != null && ($input['price_order']) != ""){
            $lodge->join('currencies as c2', 'c2.id', '=', 'lodges.currency_id')->selectRaw('c2.rate*lodges.price as lodges.acc_price')
            ->OrderBy('price',$input['price_order']);
        }
        if((array_key_exists('rate_order',$input)) && ($input['rate_order']) != null && ($input['rate_order']) != ""){
            $lodge->join('rates', 'rates.owner_id', '=', 'lodges.user_id')->selectRaw('lodges.*,lodges.id,avg(rates.overall) as total_rate')->OrderBy('total_rate',$input['rate_order'])
            ->groupBy(
                'lodges.id','process','unit_type_id','bedrooms','bathrooms','size','floor_no','price',
                'currency_id','period_id','governrate_id','area_id','compound_id','title','desc','img','floor_plan',
                'ref_code','status_id','count_views','count_like','expiry_date','notes','lodges.created_at','coordinates','lodges.updated_at','lodges.deleted_at',
                'lodges.user_id','lodges.iframe'
                );
        }
        //filter and sort section
        if(Auth::user() != null){
            $lodge->leftjoin('rates',function ($join){
                $join->on('rates.lodge_id', '=', 'lodges.id')
                ->where('rates.rater_id',Auth::user()->id);
            })->select("lodges.*",
                DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = '.Auth::user()->id.' THEN "1"
                        ELSE "0"
                        END) AS is_rated '));
        }else{
            $lodge->leftjoin('rates',function ($join){
                $join->on('rates.lodge_id', '=', 'lodges.id')
                ->where('rates.rater_id','0');
            })->select("lodges.*",
                DB::raw('(CASE
                        WHEN rates.rater_id != "null" AND rater_id = 0 THEN "1"
                        ELSE "0"
                        END) AS is_rated '));
        }
        $arrLodges = $lodge->where('status_id','=','2')->OrderBy('lodges.id','DESC')->paginate(5);
        //         dd($arrLodges);
        $arr = $this->getLodgeDetails($arrLodges,$input['lang_id']);
        return $arr;
    }
    
    public function getLodgeDetails($arrLodges,$lang=null){
        foreach ($arrLodges as $index=>$lodge){
            if(isset($arrLodges[$index]['acc_price'])){
            unset($arrLodges[$index]['acc_price']);
            }
            $arrLodges[$index]['total_rate'] = floatval(number_format(DB::table('rates')->where('owner_id',$arrLodges[$index]['user_id'])->avg('overall'),1));
            if($arrLodges[$index]['total_rate'] == null){
                $arrLodges[$index]['total_rate'] = 0 ;
            }
            if($arrLodges[$index]['img'] != null){
                $arrLodges[$index]['img'] = json_decode($arrLodges[$index]['img']);
                $arrImg = array();
                $arrPlan = array();
                foreach ($arrLodges[$index]['img'] as $index2=>$img){
                    $arrImg [] = str_replace('\\', '/', MediaUrl::getImage().$arrLodges[$index]['img'][$index2]);
                }
                $arrLodges[$index]['img'] = $arrImg;
            }
            if($arrLodges[$index]['floor_plan']!= null){
                $arrLodges[$index]['floor_plan'] = json_decode($arrLodges[$index]['floor_plan']);
                foreach ($arrLodges[$index]['floor_plan'] as $index2=>$img){
                    $arrPlan [] = str_replace('\\', '/', MediaUrl::getImage().$arrLodges[$index]['floor_plan'][$index2]);
                }
                $arrLodges[$index]['floor_plan'] = $arrPlan;
            }
            $objOption = new Lodgeoption();
            $objUnitType = new Unittype();
            $objCurr = new Currency();
            $objPeriod = new Period();
            $objGov = new Governrate();
            $objArea = new Area();
            $objComp = new Compound();
            $objStatus = new Status();
            $arrLodges[$index]['Features']= $objOption->getLodgeOptions($arrLodges[$index]['id'],$lang);
            $arrLodges[$index]['unit_type_id'] = $objUnitType->where('id',$arrLodges[$index]['unit_type_id'])->first()->translate($lang,'en')->name;
            $arrLodges[$index]['currency_id'] = $objCurr->where('id',$arrLodges[$index]['currency_id'])->first()->translate($lang,'en')->name;
            if($arrLodges[$index]['period_id']!= null){
                $arrLodges[$index]['period_id'] = $objPeriod->where('id',$arrLodges[$index]['period_id'])->first()->translate($lang,'en')->name;
            }
            $arrLodges[$index]['governrate_id'] = $objGov->where('id',$arrLodges[$index]['governrate_id'])->first()->translate($lang,'en')->name;
            $arrLodges[$index]['area_id'] = $objArea->where('id',$arrLodges[$index]['area_id'])->first()->translate($lang,'en')->name;
            ($arrLodges[$index]['compound_id'] != null) ? $arrLodges[$index]['compound_id'] = $objComp->where('id',$arrLodges[$index]['compound_id'])->first()->translate($lang,'en')->name : null ;
            $arrLodges[$index]['coordinates'] = $lodge->getCoordinates();
            $arrLodges[$index]['status_id'] = $objStatus->where('id',$arrLodges[$index]['status_id'])->first(['id','name','color'])->translate($lang,'en');
            $flag = ($arrLodges[$index]['status_id']->id == 5 || $arrLodges[$index]['status_id']->id == 7) ? 1 : 0 ;
            $arrLodges[$index]['show_status'] = $flag ;
            
            $deactivate_flag = ($arrLodges[$index]['status_id']->id == 2) ? 1 : 0 ;
            $arrLodges[$index]['deactivate_flag'] = $deactivate_flag ; 
            
            $repost_flag = ($arrLodges[$index]['status_id']->id == 3 || $arrLodges[$index]['status_id']->id == 5 ||$arrLodges[$index]['status_id']->id == 7) ? 1 : 0 ;
            $arrLodges[$index]['repost_flag'] = $repost_flag ;
            $objBadge = new Badge();
            $arrLodges[$index]['badges'] = $objBadge->getBadgeByUser($arrLodges[$index]['user_id'],$lang);
            if(Auth::user() == null){
                $arrLodges[$index]['channel_name'] = null;
                
            }else{
                $arrLodges[$index]['channel_name'] = Auth::user()->id.'-'.$arrLodges[$index]['user_id'].'-'.$arrLodges[$index]['ref_code'];
            }
            $arrLodges[$index]['user_id'] = DB::table('users')->where('id',$arrLodges[$index]['user_id'])->first(['id','email','phone','name']);
            $objLike = new Like();
            if(Auth::user() == null){
                $isLike = $objLike->isLike($lodge['id'],null);
            }else{
                $isLike = $objLike->isLike($lodge['id'],Auth::user()->id);
            }
            $arrLodges[$index]['isLike'] = $isLike ;
            Carbon::setLocale($lang);
            $arrLodges[$index]['created'] = Carbon::now()->subHour(Carbon::now()->diffInHours($arrLodges[$index]['created_at']))->diffForHumans();
            $objTrans = new Packagetransaction();
            $arrLodges[$index]['duration'] = $objTrans->getLodgeDuration($arrLodges[$index]['id']);
            if($arrLodges[$index]['floor_no'] != null){
                $arr = $arrLodges[$index]['Features'];
                $obj = new \stdClass();
                $obj->id = 0;
                $obj->icon = str_replace('\\', '/', MediaUrl::getUrl().'floor.png');
                $obj->feature_name = $arrLodges[$index]['floor_no'];
                array_unshift($arr,$obj);
                $arrLodges[$index]['Features'] = $arr;
            }
        }
        return $arrLodges;
        
    }
    
    public function listMyLodges($lang=null){
        $arrLodges = $this->where('user_id',Auth::user()->id)->paginate(5);
        $arrLodges = $this->getLodgeDetails($arrLodges,$lang);
        return $arrLodges ;
    }
    
    public function editLodge($request){
        $result = $this->where('id',$request['lodge_id'])->update([
            'process'=>$request['process']
            ,'unit_type_id'=>$request['unit_type_id']
            ,'bedrooms'=>$request['bedrooms']
            ,'bathrooms'=>$request['bathrooms']
            ,'size'=>$request['size']
            ,'floor_no'=>$request['floor_no']
            ,'price'=>$request['price']
            ,'currency_id'=>$request['currency_id']
            ,'period_id'=>$request['period_id']
            ,'governrate_id'=>$request['governrate_id']
            ,'area_id'=>$request['area_id']
            ,'compound_id'=>$request['compound_id']
            ,'title'=>$request['title']
            ,'desc'=>$request['desc']
            ,'coordinates'=>(DB::raw('ST_GeomFromText(\'POINT('.$request['lat'].' '.$request['lng'].')\')'))
            ,'ref_code'=>$request['ref_code']
            ,'status_id'=>1
            ,'user_id'=>Auth::user()->id
            ,'img'=>$request['gallery']
            ,'floor_plan'=>$request['floor']
            ,'iframe'=>"<iframe
          width="."100%"."
          height="."100%"."
          frameborder=0"."
          scrolling=yes"."
          marginheight=0"."
          marginwidth=0"."
          src="."https://maps.google.com/maps?q=".$request['lat'].",".$request['lng']."&hl=es;z=1&amp;output=embed"."></iframe>"
            ]);
        $objApn = new Apntoken();
        $notifi = $objApn->sendNotifi(Auth::user()->id,'Lodge','Your Lodge #'.$request['ref_code'].' is In Review and we will notifi you once it is approved');
        return  $this ;
    }
    public function addLodgeAp($request){
        $this->process = $request['process'];
        $this->unit_type_id = $request['unit_type_id'];
        $this->bedrooms = $request['bedrooms'];
        $this->bathrooms = $request['bathrooms'];
        $this->size = $request['size'];
        $this->floor_no = $request['floor_no'];
        $this->price = $request['price'];
        $this->currency_id = $request['currency_id'];
        $this->period_id = $request['period_id'];
        $this->governrate_id = $request['governrate_id'];
        $this->area_id = $request['area_id'];
        $this->compound_id = $request['compound_id'];
        $this->title = $request['title'];
        $this->desc = $request['desc'];
        $this->coordinates = (DB::raw('ST_GeomFromText(\'POINT('.$request['coordinates']['lat'].' '.$request['coordinates']['lng'].')\')'));
        $this->ref_code = $request['ref_code'];
        $this->status_id = 1 ;
        $this->user_id = $request['user_id'] ;
        $this->img = $request['gallery'];
        $this->floor_plan = $request['floor'];
        $this->iframe = "<iframe
          width="."550"."
          height="."170"."
          frameborder=0"."
          scrolling=yes"."
          marginheight=0"."
          marginwidth=0"."
          src="."https://maps.google.com/maps?q=".$request['coordinates']['lat'].",".$request['coordinates']['lng']."&hl=es;z=1&amp;output=embed"."></iframe>";
        $result = $this->save();
        return  $this ;
    }
    public function editLodgeAp($request){
        $result = $this->where('id',$request['lodge_id'])->update([
            'process'=>$request['process']
            ,'unit_type_id'=>$request['unit_type_id']
            ,'bedrooms'=>$request['bedrooms']
            ,'bathrooms'=>$request['bathrooms']
            ,'size'=>$request['size']
            ,'floor_no'=>$request['floor_no']
            ,'price'=>$request['price']
            ,'currency_id'=>$request['currency_id']
            ,'period_id'=>$request['period_id']
            ,'governrate_id'=>$request['governrate_id']
            ,'area_id'=>$request['area_id']
            ,'compound_id'=>$request['compound_id']
            ,'title'=>$request['title']
            ,'desc'=>$request['desc']
            ,'coordinates'=>(DB::raw('ST_GeomFromText(\'POINT('.$request['coordinates']['lat'].' '.$request['coordinates']['lng'].')\')'))
            ,'ref_code'=>$request['ref_code']
            ,'status_id'=>$request['status_id']
            ,'user_id'=>$request['user_id']
            ,'img'=>$request['gallery']
            ,'floor_plan'=>$request['floor']
            ,'iframe'=>"<iframe
          width="."100%"."
          height="."100%"."
          frameborder=0"."
          scrolling=yes"."
          marginheight=0"."
          marginwidth=0"."
          src="."https://maps.google.com/maps?q=".$request['coordinates']['lat'].",".$request['coordinates']['lng']."&hl=es;z=1&amp;output=embed"."></iframe>"
        ]);
//         $objApn = new Apntoken();
//         $notifi = $objApn->sendNotifi(Auth::user()->id,'Lodge','Your Lodge #'.$request['ref_code'].' is In Review and we will notifi you once it is approved');
        return  $this ;
    }
}