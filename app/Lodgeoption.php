<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Lodgeoption extends Model
{
    protected $fillable = [
        'id', 'lodge_id','option_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    public function getLodgeOptions($lodgeId,$lang=null){
        $arrOptions = $this->where('lodge_id',$lodgeId)->get(['option_id'])->toArray();
        $objOption = new Option();
        $objFeature = new Feature();
        $arr2 = array();
        foreach ($arrOptions as $index=>$optionId){
            $arr = array();
            $arr['options'] = $objOption->where('id',$optionId)->where('name','!=',"No")->get(['id','name','feature_id']);
            if(count($arr['options']) > 0){
                $arr2['id'] = $arr['options'][0]->id;
                $arr['feature'] = $objFeature->where('id',$arr['options'][0]->feature_id)->first(['id','name','icon'])->translate($lang,'en');
                $arr2['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$arr['feature']->icon); ;
                ($arr['options'][0]->name == 'yes') ? $arr2['feature_name'] = $arr['feature']->name : $arr2['feature_name'] = $objOption->where('id',$arr['options'][0]->id)->first(['id','name'])->translate($lang,'en')->name;
                $arrOptions[$index] = $arr2 ;
            }else{
                 unset($arrOptions[$index]);
            }
            
        }
        
        return array_values($arrOptions); 
    }
    
    public function getChosenOptions($lodgeId,$lang=null){
        $arrOptions = $this->where('lodge_id',$lodgeId)->get(['option_id'])->toArray();
        $objFeature = new Feature();
        $arrFeatures = $objFeature->listFeatures($lang);
        foreach ($arrFeatures as $index=>$feature){
            $feature['id'] = intval($feature['id']);
            $arrFeatures[$index]['icon'] = str_replace('\\', '/', MediaUrl::getUrl().$arrFeatures[$index]['icon']);
            $objOption = new Option();
            $arrFeatures[$index]['options'] = $objOption->getOptionByFeatId($feature['id'],$lang);
            foreach ($arrFeatures[$index]['options'] as $obj){
                $obj['id'] = intval($obj['id']);
            }
            foreach ($arrOptions as $objOption){
                foreach ($arrFeatures[$index]['options'] as $obj){
                    $obj['id'] = intval($obj['id']);
                    ($objOption['option_id'] == $obj['id']) ? $arrFeatures[$index]['chosen_option'] = intval($obj['id']) : null ;
                }
            }
            if(!isset($arrFeatures[$index]['chosen_option'])){
                $arrFeatures[$index]['chosen_option'] = null ;
            }
        }
        return $arrFeatures;
    }
   
}
