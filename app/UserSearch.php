<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class UserSearch extends Model
{
    protected $fillable = [
        'id', 'process', 'unit_type_id','governrate_id','area_id','compound_id','price_max','price_min','keyword','user_id'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    public function saveSearch($input){
//         dd(array_key_exists($input['unit_type_id'],$input));
        $this->process = $input['process'];
        if(isset($input['type']) && count($input['type']) > 0){
            $this->unit_type_id = json_encode($input['type']);
        }
        if(isset($input['gov_id']) && count($input['gov_id']) > 0){
            $this->governrate_id = json_encode($input['gov_id']);
        }
        if(isset($input['area_id']) && count($input['area_id']) > 0){
            $this->area_id = json_encode($input['area_id']);
        }
        if(isset($input['compound_id']) && count($input['compound_id']) > 0){
            $this->compound_id = json_encode($input['compound_id']);
        }
        if(isset($input['keyword']) && $input['keyword'] != null && $input['keyword'] != ""){
            $this->keyword = $input['keyword'];
        }
        if(isset($input['price_max']) && $input['price_max'] != null && $input['price_max'] != ""){
            $this->price_max = $input['price_max'];
        }
        if(isset($input['price_min']) && $input['price_min'] != null && $input['price_min'] != ""){
            $this->price_min= $input['price_min'];
        }
        $this->user_id = Auth::user()->id ;
        $this->save();
    }
}
