<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Validator;


class Code extends Model
{
    protected $fillable = [
        'id', 'phone', 'code','ttl'
    ];
    
    protected $hidden = [
        'created_at', 'deleted_at','updated_at'
    ];
    
    public function checkLogs($phone){
        $res = $this->where('phone',$phone)->get()->last();
        if($res!=null){
            if(Carbon::parse($res['ttl'])->diffInMinutes($res['updated_at']) >= 3){
                if(Carbon::now()->diffInDays($res['updated_at']) >= 1){
                    return true ;
                }else{
                    return false;
                }
            }
        }
        return true ;
    }
    
    public function generateCode($phone){
        $res = $this->where('phone',$phone)->get()->last();
        if($res !=null){
            if(Carbon::parse($res['ttl'])->diffInMinutes($res['updated_at']) == 1){
                $minutes = 2 ;
            }else if(Carbon::parse($res['ttl'])->diffInMinutes($res['updated_at']) == 2){
                $minutes = 3 ;
            }else{
                    $minutes = 1 ;
            }
        }else{
            $minutes = 1 ;
        }
        $this->phone = $phone;
        $this->code = $this->getCode();
        
        $this->ttl = Carbon::now()->addMinute($minutes);
        $this->save();
        //         $this->send($request['phone'],'your verification code is '.$objCode->code);
        return $this->code ; 
    }
    
    public function getCode(){
        $code = random_int(1000,9999);
        $validator = Validator::make(['code'=>$code],['code'=>'unique:codes']);
        
        if($validator->fails()){
            return $this->getCode();
        }
        
        return $code;
    }
    
    public function send($recipientNumber,$messageContent){
        
        $fields = array
        (
            'from'    => 'Lodge',
            'to'  => $recipientNumber,
            'text' => $messageContent
        );
        
        $headers = array
        (
            'Authorization: Basic U3BvcnRveWE6Q3Jpc3RpYW5vXzIw',
            'Content-Type: application/json',
            'Accept: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://merej.api.infobip.com/sms/2/text/single' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        
    }
    public function checkCurrentCode($phone){
      $entry = $this->where('phone',$phone)->where('ttl','>=',Carbon::now())->first();
      if($entry){
          return 'Retry after '.Carbon::now()->diffInMinutes($entry['ttl']).' minutes '.Carbon::now()->diffInSeconds($entry['ttl']).' seconds';
      }else{
          return 'true';
      }
        
    }
    
}
