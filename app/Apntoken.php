<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelFCM\Message\Topics;

use LaravelFCM\Message\OptionsBuilder;

use LaravelFCM\Message\PayloadDataBuilder;

use LaravelFCM\Message\PayloadNotificationBuilder;

use FCM;


class Apntoken extends Model
{
    protected $table = 'apntokens';
    
    protected $fillable = ['id','user_id','token'];
    
    protected $hidden = ['created_at','updated_at','deleted_at'];
    
    
    public function sendNotifi($user_id,$title,$content){
        
        $tokenofuser = $this->where('user_id',$user_id)->groupBy('token')->pluck('token')->toArray();
        if(count($tokenofuser)>0){
            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($content)
            ->setSound('default');
            $notification = $notificationBuilder->build();
            $downstreamResponse = FCM::sendTo($tokenofuser, null, $notification, null);
            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
            return $downstreamResponse;
        }
        
        else
            
            return null;
    }
   
}
