<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization,enctype");
$middleware = ['api'];
if (\Request::header('Authorization') == null){
    
}else{
    $middleware = array_merge(['auth:api']);
}
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::group(['prefix' => 'user'], function() {
        	Route::post('register', 'UserController@Register');
        	Route::post('checkAccount', 'UserController@CheckAccount');
        	Route::post('login', 'UserController@Login');
        	Route::post('facebook/login', 'UserController@Facebook');
        	
    });
    
        Route::get('unitTypes/list', 'UnitTypeController@ListUnittypes');
        Route::get('features/list', 'FeatureController@ListFeatures');
        Route::get('currencies/list', 'CurrencyController@ListCurrencies');
        Route::get('periods/list', 'PeriodController@ListPeriods');
        Route::get('governrates/list', 'GovernrateController@ListGovernrates');
        Route::post('area/list', 'AreaController@ListAreaByGovId');
        Route::get('area/featured/list', 'AreaController@ListFeaturedAreas');
        Route::post('compounds/list', 'CompoundController@ListCompByAreaId');
        Route::post('coordinates/get', 'GovernrateController@GetCoordinatesByAdd');
        
        Route::get('packages/list', 'PackageController@ListPackages');
        Route::get('intro/list', 'IntroController@ListIntro');
        
        Route::group(['prefix' => 'lodge','middleware' => 'auth:api'], function() {
            
            Route::post('/add', 'LodgeController@AddLodge');
            Route::post('/image/upload', 'UploaderController@UploadLodgeImage');
//             Route::post('/list', 'LodgeController@ListLatest');
//             Route::post('/list/by/user', 'LodgeController@ListLodgesByUser');
            Route::post('/like/submit', 'LikeController@SubmitLike');
//             Route::post('/search', 'LodgeController@SearchLodge');
            Route::post('/fav/get', 'LodgeController@ListFavLodges');
            Route::post('/my/get', 'LodgeController@ListMyLodges');
            Route::post('/deactivate', 'LodgeController@DeactivateLodge');
            Route::post('/image/delete', 'UploaderController@DeleteLodgeImage');
            Route::post('/edit', 'LodgeController@EditLodge');
            Route::get('/get/badges', 'BadgeController@GetUserBadges');
            Route::post('/repost', 'LodgeController@ReactivateLodge');
            
        });
        
            Route::group(['prefix' => 'lodge','middleware' => $middleware], function() {
                Route::post('/list', 'LodgeController@ListLatest');
                Route::post('/list/by/user', 'LodgeController@ListLodgesByUser');
                Route::post('/search', 'LodgeController@SearchLodge'); 
                Route::post('/get', 'LodgeController@GetLodge');
                
            });
        
        Route::group(['prefix' => 'config'], function() {
            Route::get('/list', 'TipController@GetConfig');
        });
            
        Route::group(['prefix' => 'lodge'], function() {
            Route::post('/add/view', 'LodgeController@AddView');
        });
                
        Route::group(['prefix' => 'user','middleware' => 'auth:api'], function() {
            Route::post('profile/get', 'UserController@GetProfile');
            Route::post('profile/modify', 'UserController@EditProfile');
        });
        
        Route::group(['prefix' => 'subject','middleware' => 'auth:api'], function() {
            Route::get('/get', 'SubjectController@GetSubjects');
        });
        
        Route::group(['prefix' => 'help','middleware' => 'auth:api'], function() {
            Route::post('/issue/submit', 'ComplainController@SubmitComplain');
        });
        
        Route::group(['prefix' => 'user','middleware' => 'auth:api'], function() {
            Route::get('/lastSsearch/get', 'UsersearchController@GetLastSearch');   
            Route::post('/submit/rate', 'RateController@SubmitRate');
        });
        
        Route::group(['prefix' => 'badges'], function() {
            Route::get('/list', 'BadgeController@ListBadges');
        });
            
        Route::group(['prefix' => 'apn','middleware' => $middleware], function() {
            Route::post('/add', 'ApntokenController@AddApn');
            Route::post('/delete', 'ApntokenController@DeleteApn');
            
        });
        
        Route::group(['prefix' => 'notifi'], function() {
            Route::post('/send', 'ApntokenController@SendNotifi');
        });
        
        Route::group(['prefix' => 'reasons','middleware' => 'auth:api'], function() {
            Route::get('/get', 'ReasonController@ListReasons');
        });
        
        Route::group(['prefix' => 'password'], function() {
            Route::post('/reset', 'PasswordResetController@Create');
        });
        
        Route::group(['prefix' => 'password'], function() {
            Route::get('find/{token}', 'PasswordResetController@Find');
            Route::post('updatePass', ['as' => 'password/updatePass','uses' => 'PasswordResetController@reset']);
        });
        
            Route::post('/test', 'PackageTransactionController@Chrone');
        
        Route::group(['prefix' => 'suppliers'], function() {
            Route::get('categories/list', 'SuppliercategoryController@GetSupplierCategories');
        });
        
            Route::group(['prefix' => 'chat','middleware' => 'auth:api'], function() {
                Route::post('create', 'twilioController@CreateChannel');
                Route::post('token/generate', 'twilioController@GenerateToken');
            });
                Route::post('deleteChannel', 'twilioController@DeleteChannel');
                
    Route::group(['prefix' => 'sms'], function() {
        Route::post('/send', 'CodeController@GenerateCode');
        Route::post('/validate', 'CodeController@ValidateCode');
    });
    
        Route::group(['prefix' => 'suppliers'], function() {
            Route::post('categories/list', 'SuppliercategoryController@GetSupplierCategories');
            
            Route::post('stores/listById', 'StoreController@GetStoresByCatId');
            Route::post('stores/get', 'StoreController@GetStoreById');
            Route::post('stores/filter', 'StoreController@FilterStores');
            
            Route::post('offers/cat/list', 'OfferController@GetOffersByCatId');
            Route::post('offers/store/list', 'OfferController@GetOffersByStoreId');
            Route::post('offers/get', 'OfferController@GetOfferById');
            
            Route::post('products/listById', 'ProductController@GetProductsByOfferId');
        });
            
            Route::group(['prefix' => 'developers'], function() {
                Route::post('list', 'DeveloperController@GetDevelopers');
                Route::post('get', 'DeveloperController@GetDeveloperById');
            });