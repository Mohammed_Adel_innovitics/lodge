<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Global stylesheets -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

	<link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css">

	<link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css">

	<link href="{{ asset('css/colors.css') }}" rel="stylesheet" type="text/css">
	
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	


	<title>Lodge</title>




</head>
@if($result == 1)
<body class="login-container" style="background-image: url({{asset('images/lodge_backgroud.jpg')}});">



	<!-- Main navbar -->

	 <div class="heading" style="

    margin-left: 14%;

    margin-top: 2%;

    margin-bottom: -5%;

">

						<img src="{{ asset('images/logo.png') }}" class="content-group mt-10" alt="" style="width: 190px;">  

					</div> 

	<!-- /main navbar -->





	<!-- Page container -->

	<div class="page-container">



						

					 



		<!-- Page content -->

		<div class="page-content">





			<!-- Main content -->

			<div class="content-wrapper">



				<!-- Content area -->

				<div class="content">



					<!-- Password recovery -->

					<form action="{{ route('password/updatePass') }}" method="post">

						<div class="panel panel-body login-form">

							<div class="text-center">

								<div class=" "><i class=" "></i></div>

								<h2 class="content-group">Reset Password

								<br/>



								 <small class="display-block text-center">We have logged you in automatically so you can reset your password here. Please type it twice to avoid any typos.</small></h2>

							</div>



							<div class="form-group has-feedback">

								

								<input type="password" class="form-control" name = "password" onkeyup='check();' id="password"

								pattern = ".{6,}" title = "Must contain at least 6 or more characters" placeholder="New Password" onkeyup='check();' >



								<div class="form-control-feedback">

									<i class="  text-muted"></i>

								</div>

							 <br/>

							

								<input type="password" class="form-control"  pattern=".{6,}" title="Must contain at least 6 or more characters" placeholder="Confirm Password"  name ="confirm_password" id="confirm_password" onkeyup='check();' >

								 <span id='message'></span>

								<div class="form-control-feedback">

									<i class="  text-muted"></i>

								</div>

							</div>

							<script type="text/javascript">

							var check = function() {

								  if (document.getElementById('password').value ==

								    document.getElementById('confirm_password').value) {

								    document.getElementById('message').style.color = 'green';

								    document.getElementById('message').innerHTML = 'matching';

								  } else {

								    document.getElementById('message').style.color = 'red';

								    document.getElementById('message').innerHTML = 'not matching';

								  }

								}



							</script>

								

								

								<input type="hidden" value="{{$usermail}}" name="uemail">

								<div class="form-control-feedback">

									<i class="  text-muted"></i>

								</div>

							  



							<button type="submit" class="btn bg-blue " style="background-color: #1da1f2;color: #fff;border-radius: 0px;width: 100%;">Reset </button>

						</div>

					</form>

					<!-- /password recovery -->





					<!-- Footer -->

					 

					<!-- /footer -->



				</div>

				<!-- /content area -->



			</div>

			<!-- /main content -->



		</div>

		<!-- /page content -->



	</div>

	<!-- /page container -->



</body>

</html>



@elseif($result == 0) 
<body class="login-container" style="background-image: url({{asset('images/lodge_backgroud.jpg')}});">



	<!-- Main navbar -->

	 <div class="heading" style="

    margin-left: 14%;

    margin-top: 2%;

    margin-bottom: -5%;

">

						<img src="{{ asset('images/logo.png') }}" class="content-group mt-10" alt="" style="width: 190px;">  

					</div> 

	<!-- /main navbar -->





	<!-- Page container -->

	<div class="page-container">



						

					 



		<!-- Page content -->

		<div class="page-content">





			<!-- Main content -->

			<div class="content-wrapper">



				<!-- Content area -->

				<div class="content">



					<!-- Password recovery -->

					<form action="index.html">



						<div class="panel panel-body login-form">



							<div class="text-center">



								<div class=" "><i class=" "></i></div>



								<h1 class="content-group">Reset Password



								<br/>







								 <small class="display-block" style="color: #fd1100;



                                 ">Your password reset link is not valid, or already used.</small></h1>



							</div>







							 







							 



						</div>



					</form>

					<!-- /password recovery -->





					<!-- Footer -->

					 

					<!-- /footer -->



				</div>

				<!-- /content area -->



			</div>

			<!-- /main content -->



		</div>

		<!-- /page content -->



	</div>

	<!-- /page container -->





</body>

</html>





@elseif($result == 2)

<body class="login-container" style="background-image: url({{asset('images/lodge_backgroud.jpg')}});">



	<!-- Main navbar -->

	 <div class="heading" style="

    margin-left: 14%;

    margin-top: 2%;

    margin-bottom: -5%;

">

						<img src="{{ asset('images/logo.png') }}" class="content-group mt-10" alt="" style="width: 190px;">  

					</div> 

	<!-- /main navbar -->





	<!-- Page container -->

	<div class="page-container">



						

					 



		<!-- Page content -->

		<div class="page-content">





			<!-- Main content -->

			<div class="content-wrapper">



				<!-- Content area -->

				<div class="content">



					<!-- Password recovery -->

					<form action="index.html">



						<div class="panel panel-body login-form">



							<div class="text-center">



								<div class=" "><i class=" "></i></div>



								<h1 class="content-group">Reset Password



								<br/>







								 <small class="display-block" style="color: #fd1100;



                                 ">Password Changed Successfully.</small></h1>



							</div>







							 







							 



						</div>



					</form>

					<!-- /password recovery -->





					<!-- Footer -->

					 

					<!-- /footer -->



				</div>

				<!-- /content area -->



			</div>

			<!-- /main content -->



		</div>

		<!-- /page content -->



	</div>

	<!-- /page container -->





</body>

</html>



@endif





