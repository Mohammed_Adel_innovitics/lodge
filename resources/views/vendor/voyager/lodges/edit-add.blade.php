@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif
                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->display_name }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add')])
                                    @elseif ($row->type == 'relationship')
                                    		@if($row->id == '155')
                                            		@if($add == 'true')
                                            		<select
                                                        class="form-control select2"  name="{{ $row->details->column }}"
                                                    required>
                                                    </select>
                                                    @else
                                                    <select
                                                    class="form-control select2" name="{{ $row->details->column }}"
                                                    >
                                                    @php
                        							$model = app($row->details->model);
                        							$query = $model::where('governrate_id',$dataTypeContent->governrate_id)->get();
                    								@endphp
                    								@foreach($query as $relationshipData)
                       								 <option value="{{ $relationshipData->{$row->details->key} }}" @if($dataTypeContent->{$row->details->column} == $relationshipData->{$row->details->key}){{ 'selected="selected"' }}@endif>{{ $relationshipData->{$row->details->label} }}</option>
                    								@endforeach
                                                    </select>
                                                    @endif
                                            @elseif($row->id == '156')
                                                    @if($add == 'true')
                                                    @php
                                                    $eedit = 0;
                                                    @endphp
                                            		<select
                                                        class="form-control select2"  name="{{ $row->details->column }}"
                                                    >
                                                    </select>
                                                    @else
                                                    <select
                                                    class="form-control select2" name="{{ $row->details->column }}"
                                                    >
                                                    @php
                                                    $eedit = 1;
                        							$model = app($row->details->model);
                        							$query = $model::where('area_id',$dataTypeContent->area_id)->get();
                    								@endphp
                    								@if(!$row->required)
                     							   <option value="">{{__('voyager::generic.none')}}</option>
                    								@endif
                    								@foreach($query as $relationshipData)
                       								 <option value="{{ $relationshipData->{$row->details->key} }}" @if($dataTypeContent->{$row->details->column} == $relationshipData->{$row->details->key}){{ 'selected="selected"' }}@endif>{{ $relationshipData->{$row->details->label} }}</option>
                    								@endforeach
                                                    </select>
                                                    @endif
                                    		@else
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                        @endif
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                            <h4>Lodge Features</h4>
                            <br>
                            <br>
                            <table style="border-collapse: collapse;width: 100%;">
							<tr>
	                       
  	                        @foreach($arrFeatures as $index=>$objFeat)
  	                        <td>
  	                        <img src="{{$objFeat->icon}}" style="max-height: 50%;" >
  	                        <p>{{$objFeat->name}}</p>
                            
                            @foreach($objFeat['options'] as $objOption)
            				<input type="radio" name="feature[{{$index}}]" value="{{$objOption->id}}" @if(isset($objFeat['chosen_option']) && $objOption->id == $objFeat['chosen_option']) checked="checked" @endif>{{$objOption->name}}<br>
        					@endforeach
        					</td>
							@endforeach
                        </tr>
                        </table>

                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
            $("select[name='governrate_id']").change(function(){
        		$("select[name='area_id'] option").remove();
        		$("select[name='compound_id'] option").remove();
        	    var selectedCountry = $(this).children("option:selected").val();
        	    if({{$eedit}} == '0'){
        	    	var url = '../../api/area/list';   
            	    }else{
            	    	var url = '../../../api/area/list';
                	    }
        	    $.ajax({
            	    
        	        url : url,
        	        type : 'post',
        	        data : {
        	            'gov_id' : selectedCountry
        	        },
        	        dataType:'json',
        	        success : function(data) { 
        	        	for (var i = 0; i < data.result.length; i++) {
        	        		$("select[name='area_id']").append('<option value="' + data.result[i].id + '">' + data.result[i].name + '</option>');
        	            }
        	        	$("select[name='compound_id'] option").remove();
                	    if({{$eedit}} == '0'){
                	    	var url2 = '../../api/compounds/list';   
                    	    }else{
                    	    	var url2 = '../../../api/compounds/list';
                        	    }
                	    $.ajax({
                	        url : url2,
                	        type : 'post',
                	        data : {
                	            'area_id' : $("select[name='area_id']").children("option:selected").val()
                	        },
                	        dataType:'json',
                	        success : function(data) { 
                	        	for (var i = 0; i < data.result.length; i++) {
                	        		$("select[name='compound_id']").append('<option value="' + data.result[i].id + '">' + data.result[i].name + '</option>');
                	            }             
                	        },
                	        error : function(request,error)
                	        {
                	            alert("Request: "+JSON.stringify(request));
                	        }
                	    });             
        	        },
        	        error : function(request,error)
        	        {
        	            alert("Request: "+JSON.stringify(request));
        	        }
        	    });
        	});
            $("select[name='area_id']").change(function(){
        		$("select[name='compound_id'] option").remove();
        	    var selected = $(this).children("option:selected").val();
        	    if({{$eedit}} == '0'){
        	    	var url2 = '../../api/compounds/list';   
            	    }else{
            	    	var url2 = '../../../api/compounds/list';
                	    }
        	    $.ajax({
        	        url : url2,
        	        type : 'post',
        	        data : {
        	            'area_id' : selected
        	        },
        	        dataType:'json',
        	        success : function(data) { 
        	        	for (var i = 0; i < data.result.length; i++) {
        	        		$("select[name='compound_id']").append('<option value="' + data.result[i].id + '">' + data.result[i].name + '</option>');
        	            }             
        	        },
        	        error : function(request,error)
        	        {
        	            alert("Request: "+JSON.stringify(request));
        	        }
        	    });
        	});
        });
    </script>
@stop
